const { Client, Events, GatewayIntentBits } = require('discord.js');
const dotenv = require('dotenv');
const EventEmitter = require('events');
const path = require('path');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config({ path: path.join(__dirname, '../.local.env') });
}

const { commandList, initCommands } = require('./commands');
const { Mongo } = require('./mongo');
const { TYPES } = require('./helpers/error');
const { Queue } = require('./helpers/queue');
const { UserHandler } = require('./handlers/user');
const { SessionHandler } = require('./handlers/session');

const checkUser = async (interaction, app) => {
  const user = new UserHandler(interaction, app);
  const validated = await user.validateUser();

  if (validated.isNew) {
    await user.inviteNewUser();
  }
};

const handleCommand = async (interaction, app) => {
  const { commandName } = interaction;
  const { handler } = commandList.get(commandName) || {};

  if (!handler) {
    throw new Error(`No handler for ${commandName} command`);
  }

  try {
    await handler(interaction, app);
  } catch (e) {
    if (e.isEnkiError) {
      if (e.type === TYPES.THROTTLED) {
        app.event.emit('throttle', e);
      }

      if (e.payload.isEmbed) {
        interaction.followUp({ embeds: [e.payload.embed], ephemeral: true });
      } else {
        interaction.followUp({ content: e.payload.message, ephemeral: true });
      }
    } else {
      console.log(e);
      interaction.followUp({ content: 'Oops, looks like we ran into an issue.', ephemeral: true });
    }
  }
};

const handleModal = async (interaction, app) => {
  const handler = new SessionHandler(interaction, app);
  await handler.createSession();
};

(async () => {
  console.log('Beginning startup...');
  const event = new EventEmitter();

  console.log('Connecting to Mongo...');
  const mongo = new Mongo();
  await mongo.init();
  console.log('MongoDB connected successfully.');

  const app = {
    event,
    mongo,
    queues: {
      randomScrape: new Queue(event),
    },
    throttling: {
      throttled: false,
      timeout: 0,
      timeoutId: null,
    },
  };

  event.on('throttle', (enkiError) => {
    app.throttling.throttled = true;
    app.throttling.timeout = enkiError.details.timeout;

    if (app.throttling.timeoutId) {
      clearTimeout(app.throttling.timeoutId);
    }

    app.throttling.timeoutId = setTimeout(() => {
      event.emit('unthrottle');
    });
  });

  event.on('unthrottle', () => {
    app.throttling.throttled = false;
    app.throttling.timeout = 0;
    app.throttling.timeoutId = null;
  });

  console.log('Initializing commands...');
  await initCommands();

  const client = new Client({ intents: [GatewayIntentBits.Guilds] });

  client.once('ready', () => {
    console.log('Enki is ready!');
    console.log('==============');
  });

  client.on(Events.InteractionCreate, async (interaction) => {
    if (interaction.isChatInputCommand()) {
      await checkUser(interaction, app);
      await handleCommand(interaction, app);
      return;
    }

    if (interaction.isModalSubmit()) {
      await handleModal(interaction, app);
    }
  });

  await client.login(process.env.DISCORD_TOKEN);

  const shutdown = async () => {
    console.log('Disconnecting Mongo...');
    await mongo.client.close();
    console.log('MongoDB connection closed.');

    console.log('Disconnecting Discord...');
    await client.destroy();
    console.log('Discord connection closed.');
  };

  process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
  });

  process.on('SIGINT', () => {
    console.log('SIGINT (aka ctrl-c) received - shutting down gracefully...');
    shutdown();
  });

  process.on('SIGTERM', () => {
    console.log('SIGTERM (container stop) received - shutting down gracefully...');
    shutdown();
  });

  process.once('SIGUSR2', async () => {
    console.log('Performing graceful shutdown for nodemon...');
    await shutdown();
    process.kill(process.pid, 'SIGUSR2');
  });
})();
