/* **** */
/* Math */
/* **** */
const calculateScore = (work, averages) => {
  const { stats: { bookmarks, chapters, comments, hits, kudos } } = work;

  // calculate kudos ratio scores (used for hidden-hit works)
  const bkr = (bookmarks / (kudos || 1)) * 100;
  const ckr = ((comments / chapters) / (kudos || 1)) * 100;

  // calculate hit ratio scores, but don't use if hits is 0 or null
  const khr = (kudos / (hits || 1)) * 100;
  const bhr = (bookmarks / (hits || 1)) * 1000;
  const chr = ((comments / chapters) / (hits || 1)) * 1000;

  // untracked fandoms
  if (!averages) {
    // prefer hit ratios on smaller/newer fics in untracked fandoms
    if (hits && (!bookmarks || !comments)) {
      return Math.round(khr + bhr + chr);
    }

    // prefer kudos over hit scores for untracked fandoms
    return Math.round(bkr + ckr);
  }

  // get tracked fandom averages
  const akr = averages.kudos ? (kudos / averages.kudos) * 10 : 0;
  const abr = averages.bookmarks ? (bookmarks / averages.bookmarks) * 10 : 0;

  // fall back to kudos ratio scores for hidden-hit works
  if (!hits) {
    return Math.round(bkr + ckr + akr + abr);
  }

  // use hit ratio and average ratio scores for everything else
  return Math.round(khr + bhr + chr + akr + abr);
};

const getRandomInRange = (min, max) => Math.floor(Math.random() * ((max - min) + 1)) + min;

const getRelativeTime = (time) => {
  const date = new Date(time);
  const elapsed = date - new Date();

  const units = {
    year: 1000 * 60 * 60 * 24 * 365,
    month: 1000 * 60 * 60 * 24 * 30,
    day: 1000 * 60 * 60 * 24,
    hour: 1000 * 60 * 60,
    minute: 1000 * 60,
    second: 1000,
  };

  const found = Object.entries(units).find(([, value]) => Math.abs(elapsed) > value);
  const unit = found ? found[0] : 'second';

  const rtf = new Intl.RelativeTimeFormat('en', { numeric: 'auto' });
  return rtf.format(Math.round(elapsed / units[unit]), unit);
};

/* ********************************************************************************************** */

/* **** */
/* URLs */
/* **** */
const decodeAo3Url = (url) => decodeURI(url)
  .replace(/\*d\*/, '.')
  .replace(/\*s\*/, '/');

const getEncodedTag = (url) => {
  if (typeof url !== 'string') { return null; }
  const match = url.match(/https:\/\/archiveofourown.org\/tags\/([a-zA-Z0-9%*()']+)/);
  if (!match) { return null; }

  return match[1];
};

const getPage = (url) => {
  if (typeof url !== 'string') { return null; }
  const match = url.match(/page=([0-9]+)/);
  if (!match) { return null; }

  return match[1];
};

const getTagUrl = (url) => {
  const id = getEncodedTag(url);
  if (!id) { return null; }

  return `https://archiveofourown.org/tags/${id}`;
};

const getWorkId = (url) => {
  if (typeof url !== 'string') { return null; }
  const match = url.match(/https:\/\/archiveofourown.org\/works\/([0-9]+)/);
  if (!match) { return null; }

  return match[1];
};

/* ********************************************************************************************** */

/* ******* */
/* Strings */
/* ******* */
const getNextName = (type = '', names = []) => {
  const regex = new RegExp(`${type ? `${type} ` : ''}(\\d+)`);
  const foundCounts = names.map((name) => {
    const match = name.match(regex);
    if (!match) { return null; }

    return parseInt(match[1], 10);
  });
  const highestCount = foundCounts.reduce((max, num) => {
    if (typeof num !== 'number') { return max; }
    return Math.max(max, num);
  }, names.length);

  return `${type ? `${type} ` : ''}${highestCount + 1}`;
};

const makeRegexFriendly = (string) => {
  const toEscape = ['\\', '/', '^', '$', '.', '|', '?', '*', '+', '(', ')', '[', ']', '{', '}'];
  const fixed = string
    .split('')
    .map((c) => (toEscape.includes(c) ? `\\${c}` : c))
    .join('');

  return fixed;
};

const truncate = (input, limit = 1024, trailing = '...') => {
  if (trailing.length > limit) {
    throw new Error('Truncation: limits are applied with the trailing text considered - please provide a limit that is greater than the length of the trailing text');
  }

  if (limit > input.length) { return input; }

  return `${input.slice(0, limit - trailing.length)}${trailing}`;
};

/* ********************************************************************************************** */

module.exports = {
  calculateScore,
  decodeAo3Url,
  getEncodedTag,
  getNextName,
  getPage,
  getRandomInRange,
  getRelativeTime,
  getTagUrl,
  getWorkId,
  makeRegexFriendly,
  truncate,
};
