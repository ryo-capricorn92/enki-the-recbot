const { ComponentType } = require('discord.js');

const { RATINGS, WARNING_SHORTHANDS } = require('../constants');
const { scrapeRandomWorks } = require('../scrape');

const { DiscordDisplay } = require('../helpers/display');

const getRatingsRange = (rating, min, max) => {
  if (rating) { return [rating]; }

  const minIndex = RATINGS.findIndex((r) => r === min);
  const maxIndex = RATINGS.findIndex((r) => r === max);

  return RATINGS.slice(Math.max(minIndex, 0), (maxIndex + 1) || RATINGS.length);
};

/**
 * @typedef {import('discord.js').CommandInteraction} Interaction
 * @typedef {import('../mongo').Mongo} Mongo
 * @typedef {import('./queue').Queue} Queue
 */

class RecHandler {
  /**
   *
   * @param {Interaction} interaction
   * @param {Object} app
   * @param {Mongo} app.mongo
   * @param {Object} app.queues
   * @param {Queue} app.queues.randomScrape
   */
  constructor(interaction, app) {
    this.interaction = interaction;
    this.mongo = app.mongo;
    this.queues = app.queues;
    this.timeoutAfter = 172800000; // 48 hours
    this.tag = interaction.options.getString('tag');
    this.extratags = interaction.options.getString('extratags');
    this.rating = interaction.options.getString('rating');
    this.minRating = interaction.options.getString('minrating');
    this.maxRating = interaction.options.getString('maxrating');
    this.warningoptout = interaction.options.getString('warningoptout');
    this.warningviolence = interaction.options.getString('warningviolence');
    this.warningdeath = interaction.options.getString('warningdeath');
    this.warningnoncon = interaction.options.getString('warningnoncon');
    this.warningunderage = interaction.options.getString('warningunderage');

    this.ratingsRange = getRatingsRange(this.rating, this.minRating, this.maxRating);
  }

  async validate() {
    await this.interaction.deferReply();

    // TODO: is there any validation needed?

    return true;
  }

  async handleInvalid() {
    return this.interaction
      .editReply('Something went wrong'); // TODO
  }

  filterWarnings(value) {
    const warnings = ['warningoptout', 'warningviolence', 'warningdeath', 'warningnoncon', 'warningunderage'];
    return warnings
      .filter((warning) => this[warning] === value)
      .map((warning) => WARNING_SHORTHANDS[warning]);
  }

  async scrapeRandomFromTag(tag) {
    // find all tags under the given tag's master or synonym
    // should only be 1, but let's be safe and consider edge cases - doesn't hurt to scrape more
    const mains = await this.mongo.tag.findTagsByTags([tag], { caseInsensitive: true });

    // for ever found tag
    await Promise.all(mains.map(async (main) => {
      // scrape a random page of works from the tag
      const scrapedRes = await scrapeRandomWorks(main);

      // ingest works (update tag page count, add tracked tags, store works, etc)
      return this.mongo.work.ingestWorks(scrapedRes, main.master);
    }));
  }

  async handleExpiry(message) {
    const payload = new DiscordDisplay(message)
      .makeStatic()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handle() {
    if (!(await this.validate())) {
      return this.handleInvalid();
    }

    // consider the user's provided options
    const options = {
      tag: this.tag,
      ratingsRange: this.ratingsRange,
      requireWarnings: this.filterWarnings('require'),
      hideWarnings: this.filterWarnings('hide'),
    };

    // only try to add additional tags if they exist
    if (this.extratags) {
      options.additionalTags = this.extratags
        .split(',')
        .map((str) => str.trim());
    }

    // find a random work based off their rating score and user's read
    const user = await this.mongo.user.getUserById(this.interaction.user.id);
    const session = await this.mongo.session.getSessionById(user.activeSession);
    const work = await this.mongo.work.getRandomWorkByScore(options, session);

    // if the user provided a main tag, let's queue a random scrape of that tag
    // we can't do bulk scrapes due to AO3's throttling, but this helps keep us updated
    if (this.tag) {
      this.queues.randomScrape.enqueue(this.tag, this.scrapeRandomFromTag.bind(this));
    }

    // create the rec message
    const payload = new DiscordDisplay()
      .addRec(work)
      .addWorkOptions()
      .getPayload();

    // send the rec
    const message = await this.interaction.editReply(payload);

    // listen for button actions until the timeout
    const collector = message
      .createMessageComponentCollector({
        componentType: ComponentType.Button,
        time: this.timeoutAfter,
      });

    collector.on('collect', async (i) => {
      if (i.customId === 'markAsRead') {
        const u = await this.mongo.user.getUserById(i.user.id);
        const s = await this.mongo.session.getSessionById(u.activeSession);
        await this.mongo.session.addWorkToRead(s._id, work._id);

        i.reply({ content: 'Work marked as read!', ephemeral: true });
      }

      if (i.customId === 'markAsIgnored') {
        const u = await this.mongo.user.getUserById(i.user.id);
        const s = await this.mongo.session.getSessionById(u.activeSession);
        await this.mongo.session.addWorkToIgnored(s._id, work._id);

        i.reply({ content: 'Work marked as ignored!', ephemeral: true });
      }
    });

    collector.on('end', async () => this.handleExpiry(message));

    return message;
  }
}

module.exports = {
  getRatingsRange,
  RecHandler,
};
