const { ObjectId } = require('mongodb');

const { DiscordDisplay } = require('../helpers/display');
const { EnkiError, TYPES } = require('../helpers/error');
const { getNextName } = require('../util');

/**
 * @typedef {import('discord.js').CommandInteraction} CommandInteraction
 * @typedef {import('discord.js').ModalSubmitInteraction} ModalSubmitInteraction
 * @typedef {import('../mongo').Mongo} Mongo
 * @typedef {import('./queue').Queue} Queue
 */

class SessionHandler {
  /**
   * @param {CommandInteraction|ModalSubmitInteraction} interaction
   * @param {Object} app
   * @param {Mongo} app.mongo
   */
  constructor(interaction, app) {
    this.interaction = interaction;
    this.userId = interaction.user.id;
    this.mongo = app.mongo;
    this.timeoutAfter = 300000; // 5 mins
  }

  async createSession() {
    await this.interaction.deferReply({ ephemeral: true });

    const sessionName = this.interaction.fields.getTextInputValue('sessionName');
    const sessions = await this.mongo.session.getSessionsByUser(this.userId);
    const isUnique = sessions.every(({ name }) => name !== sessionName);

    if (!isUnique) {
      return this.interaction.editReply(`Session names must be unique - you already have a session named "${sessionName}"`);
    }

    const { insertedId } = await this.mongo.session
      .createSession(this.userId, sessionName);
    await this.mongo.user.setActiveSession(this.userId, insertedId);

    return this.interaction.editReply(`New session \`${sessionName}\` is now in use!`);
  }

  async handleBase(i) {
    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    const payload = new DiscordDisplay()
      .addSessionDetails(session, user._id)
      .addManageSessionButtons()
      .addPruneSessionButtons()
      .getPayload();

    if (i) {
      return i.update(payload);
    }

    return this.interaction.editReply(payload);
  }

  async handleNew(i) {
    const sessionList = await this.mongo.session.getSessionsByUser(this.userId);

    const currentNames = sessionList.map((s) => s.name);
    const bestName = getNextName('Session', currentNames);

    const newSessionModal = new DiscordDisplay().generateNewSessionModal(bestName);

    await i.showModal(newSessionModal);

    const modalAction = await i.awaitModalSubmit({ time: 300000 })
      .catch((e) => { console.log(e); });

    if (modalAction) {
      this.handleExpiry(i);
    }
  }

  async handleChange(i) {
    await i.deferUpdate();

    const sessionList = await this.mongo.session.getSessionsByUser(this.userId);
    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    const payload = new DiscordDisplay()
      .addSessionDetails(session, user._id)
      .addChangeSession(sessionList, session._id)
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handleSet(i) {
    const [sessionId] = i.values;
    await this.mongo.user.setActiveSession(this.userId, new ObjectId(sessionId));

    await this.handleBase(i);
  }

  async handleDelete(i) {
    await i.deferUpdate();

    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    this.sessionToDelete = session._id;

    const payload = new DiscordDisplay()
      .addSessionDetails(session, user._id)
      .addDeleteSession()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handleConfirmDelete(i) {
    await i.deferReply({ ephemeral: true });

    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    const fallbackSessionId = await this.mongo.session.findOrCreateFallback(user._id, session._id);
    const fallbackSession = await this.mongo.session.getSessionById(fallbackSessionId);

    await this.mongo.user.setActiveSession(user._id, fallbackSessionId);
    await this.mongo.session.deleteSession(session._id);

    const oldName = session.name;
    const newName = fallbackSession.name;
    const message = `\`${oldName}\` session has been successfully deleted. \`${newName}\` has been set as your active session.`;

    await this.handleExpiry(i);
    return i.editReply(message);
  }

  async handleResetRead(i) {
    await i.deferUpdate();

    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    const payload = new DiscordDisplay()
      .addSessionDetails(session, user._id)
      .addResetRead()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handleConfirmResetRead(i) {
    await i.deferReply({ ephemeral: true });

    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    await this.mongo.session.resetReadWorks(session._id);
    await this.handleExpiry(i);

    const message = `All read works for session \`${session.name}\` have been reset.`;
    return i.editReply(message);
  }

  async handleResetIgnored(i) {
    await i.deferUpdate();

    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    const payload = new DiscordDisplay()
      .addSessionDetails(session, user._id)
      .addResetIgnored()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handleConfirmResetIgnored(i) {
    await i.deferReply({ ephemeral: true });

    const user = await this.mongo.user.getUserById(this.userId);
    const session = await this.mongo.session.getSessionById(user.activeSession);

    await this.mongo.session.resetIgnoredWorks(session._id);
    await this.handleExpiry(i);

    const message = `All ignored works for session \`${session.name}\` have been reset.`;
    return i.editReply(message);
  }

  async handleExpiry(message) {
    const payload = new DiscordDisplay(message)
      .makeStatic()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handle() {
    await this.interaction.deferReply();

    const message = await this.handleBase();
    const collector = message
      .createMessageComponentCollector({ idle: this.timeoutAfter });

    collector.on('collect', async (i) => {
      if (i.user.id !== this.userId) { return null; }

      try {
        switch (i.customId) {
          case 'addNewSession':
            return this.handleNew(i);
          case 'changeSession':
            return this.handleChange(i);
          case 'setSession':
            return this.handleSet(i);
          case 'deleteSession':
            return this.handleDelete(i);
          case 'confirmSessionDelete':
            return this.handleConfirmDelete(i);
          case 'resetRead':
            return this.handleResetRead(i);
          case 'confirmReadReset':
            return this.handleConfirmResetRead(i);
          case 'resetIgnored':
            return this.handleResetIgnored(i);
          case 'confirmIgnoredReset':
            return this.handleConfirmResetIgnored(i);
          case 'cancelSessionDelete':
          case 'cancelSessionChange':
          case 'cancelReadReset':
          case 'cancelIgnoredReset':
          default:
            return this.handleBase(i);
        }
      } catch (e) {
        let error = e;
        if (!(e instanceof EnkiError)) {
          error = new EnkiError(TYPES.GENERIC, e);
        }

        if (error.payload.isEmbed) {
          i.followUp({ embeds: [e.payload.embed], ephemeral: true });
        } else {
          i.followUp({ content: e.payload.message, ephemeral: true });
        }

        return null;
      }
    });

    collector.on('end', async () => this.handleExpiry(message));
  }
}

module.exports = { SessionHandler };
