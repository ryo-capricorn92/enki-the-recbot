const { ActionRowBuilder, ButtonBuilder, ButtonStyle, ComponentType } = require('discord.js');

const { scrapeTaggedWorks, scrapeMasterTag } = require('../scrape');
const { decodeAo3Url, getEncodedTag } = require('../util');
const { EnkiError, TYPES } = require('../helpers/error');
const { DiscordDisplay } = require('../helpers/display');

/**
 * @typedef {import('discord.js').CommandInteraction} Interaction
 * @typedef {import('../mongo').Mongo} Mongo
 * @typedef {import('./queue').Queue} Queue
 */

class ScrapeHandler {
  /**
   *
   * @param {Interaction} interaction
   * @param {Object} app
   * @param {Mongo} app.mongo
   * @param {Object} app.throttling
   * @param {boolean} app.throttling.throttled
   * @param {Date} app.throttling.timeout
   */
  constructor(interaction, app) {
    if (app.throttling.throttled) {
      throw new EnkiError(TYPES.THROTTLED, { timeout: app.throttling.timeout });
    }

    this.interaction = interaction;
    this.mongo = app.mongo;
    this.timeoutAfter = 300000; // 5 mins
    this.type = interaction.options.getString('type');
    this.url = interaction.options.getString('url');
  }

  async validate() {
    const regex = /^https:\/\/archiveofourown\.org\/tags\/([A-Za-z0-9%()*']+)(\/)*(works)*(\?[A-Za-z0-9%()*=]+)*$/;
    const valid = regex.test(this.url);

    await this.interaction.deferReply({ ephemeral: !valid });

    return valid;
  }

  async handleInvalid() {
    return this.interaction
      .editReply('Invalid URL! Please provide a valid AO3 tag URL. (hint: it should start with https://archiveofourown.org/tags/)');
  }

  async addTrackedTags(work) {
    const trackedTags = await this.mongo.tag.findTrackedTags(work);

    return {
      ...work,
      trackedTags,
    };
  }

  async handleTagInit() {
    const encodedTag = getEncodedTag(this.url);

    const tag = await this.scrapeTag();
    if (!tag) { return null; }

    // scrape all pages of tops works first
    const pages = [1, 2, 3];
    const results = await Promise.all(pages.map(async (page) => {
      const pageUrl = `https://archiveofourown.org/works?page=${page}&work_search%5Bsort_column%5D=kudos_count&tag_id=${encodedTag}`;
      return scrapeTaggedWorks(pageUrl);
    }));

    // format results
    const [{ pageCount }] = results;
    const works = results.reduce((flat, ws) => ([...flat, ...ws.works]), []);

    // ingest works (update tag page count, add tracked tags, store works, etc)
    await this.mongo.work.ingestWorks({ works, pageCount }, tag.master);

    return this.interaction.editReply(`Initated the \`${tag.master}\` tag - tag and top works have been scraped.`);
  }

  async handleSingleScrape() {
    // get decoded master tag from url
    const decodedTag = decodeAo3Url(getEncodedTag(this.url));

    // scrape given url
    const scrapedRes = await scrapeTaggedWorks(this.url);

    // ingest works (update tag page count, add tracked tags, store works, etc)
    await this.mongo.work.ingestWorks(scrapedRes, decodedTag);

    return this.interaction.editReply(`Scraped one page of works from the \`${decodedTag}\` tag.`);
  }

  async handleTagScrape() {
    const decodedTag = decodeAo3Url(getEncodedTag(this.url));

    await this.scrapeTag();

    return this.interaction.editReply(`Scraped the \`${decodedTag}\` tag.`);
  }

  async handleUncommonTag(tag) {
    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('denyTag')
          .setLabel('No')
          .setStyle(ButtonStyle.Primary),
      )
      .addComponents(
        new ButtonBuilder()
          .setCustomId('acceptTag')
          .setLabel('Yes')
          .setStyle(ButtonStyle.Secondary),
      );

    const content = `
Caution: Tag \`${tag.master}\` is not a common tag and works cannot be filtered by it. 
Are you sure you want to scrape this tag?
      `;

    const message = await this.interaction.editReply({ content, components: [row] });
    const collector = message
      .createMessageComponentCollector({
        componentType: ComponentType.Button,
        time: this.timeoutAfter,
      });

    collector.on('collect', async (i) => {
      if (i.user.id !== this.interaction.user.id) { return null; }

      if (i.customId === 'acceptTag') {
        await this.mongo.tag.upsertTag(tag);
        return this.interaction.editReply({
          content: `Uncommon tag \`${tag.master}\` has been scraped. No works have been scraped for it.`,
          components: [],
        });
      }

      return this.interaction.editReply({
        content: `Uncommon tag \`${tag.master}\` has not been scraped.`,
        components: [],
      });
    });

    collector.on('end', async () => this.handleExpiry(message));

    return message;
  }

  async handleExpiry(message) {
    const payload = new DiscordDisplay(message)
      .makeStatic()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handle() {
    if (!(await this.validate())) {
      return this.handleInvalid();
    }

    switch (this.type) {
      case 'init':
        return this.handleTagInit();
      case 'single':
        return this.handleSingleScrape();
      case 'tag':
        return this.handleTagScrape();
      case 'full':
      case 'random':
      default:
        return this.interaction.editReply('This command is not supported yet.');
    }
  }

  async scrapeTag() {
    const tag = await scrapeMasterTag(this.url);

    if (!tag.isCommon) {
      await this.handleUncommonTag(tag);
      return null;
    }

    await this.mongo.tag.upsertTag(tag);
    await this.mongo.work.updateTrackedTags(tag);

    return tag;
  }
}

module.exports = { ScrapeHandler };
