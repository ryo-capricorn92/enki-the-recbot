const { ComponentType } = require('discord.js');

const { getWorkId } = require('../util');
const { scrapeWork } = require('../scrape');

const { DiscordDisplay } = require('../helpers/display');
const { EnkiError, TYPES } = require('../helpers/error');

/**
 * @typedef {import('discord.js').CommandInteraction} Interaction
 * @typedef {import('../mongo').Mongo} Mongo
 * @typedef {import('./queue').Queue} Queue
 */

class ShowHandler {
  /**
   *
   * @param {Interaction} interaction
   * @param {Object} app
   * @param {Mongo} app.mongo
   * @param {Object} app.throttling
   * @param {boolean} app.throttling.throttled
   * @param {Date} app.throttling.timeout
   */
  constructor(interaction, app) {
    if (app.throttling.throttled) {
      throw new EnkiError(TYPES.THROTTLED, { timeout: app.throttling.timeout });
    }

    this.interaction = interaction;
    this.mongo = app.mongo;
    this.timeoutAfter = 172800000; // 48 hours
    this.url = interaction.options.getString('url');
    this.getFresh = interaction.options.getBoolean('getfresh');
    this.workId = getWorkId(this.url);
  }

  async validate() {
    const regex = /^https:\/\/archiveofourown\.org\/works\/([0-9]+)(\/)*(chapters)*(\/)*([0-9]+)*$/;
    const valid = regex.test(this.url);

    await this.interaction.deferReply({ ephemeral: !valid });

    return valid;
  }

  async handleInvalid() {
    return this.interaction
      .editReply('Invalid URL! Please provide a valid AO3 work URL. (hint: it should look something like this: `https://archiveofourown.org/works/123/chapters/123)`');
  }

  async addTrackedTags(work) {
    const trackedTags = await this.mongo.tag.findTrackedTags(work);
    return {
      ...work,
      trackedTags,
    };
  }

  async ingestWork(work) {
    const withTags = await this.addTrackedTags(work);
    return this.mongo.work.upsertWork(withTags);
  }

  async getFreshWork() {
    const work = await scrapeWork(this.url);
    this.ingestWork(work); // no need to wait on the write, we already have the scrape

    return {
      _id: this.workId,
      ...work,
    };
  }

  async getStoredWork() {
    const work = await this.mongo.work.getWork(this.workId);

    if (!work) {
      return this.getFreshWork();
    }

    return work;
  }

  async handleExpiry(message) {
    const payload = new DiscordDisplay(message)
      .makeStatic()
      .getPayload();

    return this.interaction.editReply(payload);
  }

  async handle() {
    if (!(await this.validate())) {
      return this.handleInvalid();
    }

    const work = await (this.getFresh ? this.getFreshWork() : this.getStoredWork());

    // create the rec message
    const payload = new DiscordDisplay()
      .addRec(work)
      .addWorkOptions()
      .getPayload();

    // send the rec
    const message = await this.interaction.editReply(payload);

    // listen for button actions until the timeout
    const collector = message
      .createMessageComponentCollector({
        componentType: ComponentType.Button,
        time: this.timeoutAfter,
      });

    collector.on('collect', async (i) => {
      if (i.customId === 'markAsRead') {
        const user = await this.mongo.user.getUserById(i.user.id);
        const session = await this.mongo.session.getSessionById(user.activeSession);
        await this.mongo.session.addWorkToRead(session._id, this.workId);

        i.reply({ content: 'Work marked as read!', ephemeral: true });
      }
    });

    collector.on('end', async () => this.handleExpiry(message));

    return message;
  }
}

module.exports = {
  ShowHandler,
};
