const { DiscordDisplay } = require('../helpers/display');

/**
 * @typedef {import('discord.js').CommandInteraction} Interaction
 * @typedef {import('../mongo').Mongo} Mongo
 */

class UserHandler {
  /**
   * @param {Interaction} interaction
   * @param {Object} app
   * @param {Mongo} app.mongo
   */
  constructor(interaction, app) {
    this.interaction = interaction;
    this.client = interaction.client;
    this.user = interaction.user;
    this.mongo = app.mongo;
  }

  async validateUser() {
    const res = {};

    let user = await this.mongo.user.getUserById(this.user.id);
    if (!user) {
      const session = await this.mongo.session.createSession(this.user.id);
      const newUser = {
        _id: this.user.id,
        activeSession: session.insertedId,
      };
      const added = await this.mongo.user.upsertUser(newUser);
      user = await this.mongo.user.getUserById(added.upsertedId);

      res.isNew = true;
    }

    const sessions = await this.mongo.session.getSessionsByUser(this.user.id);
    if (!sessions.length) {
      await this.mongo.session.createSession(this.user.id);

      res.noSessions = true;
    }

    // TODO: check if user is admin

    return res;
  }

  async inviteNewUser() {
    const dmChannel = await this.interaction.user.createDM();

    // TODO: move this into the future support server config
    const channelID = process.env.CHANNEL_ID;
    const supportChannel = await this.client.channels.cache.get(channelID);
    const invite = await supportChannel.createInvite({
      unique: false,
      maxAge: 0,
    });

    const payload = new DiscordDisplay()
      .addWelcomeMessage()
      .getPayload();

    await dmChannel.send(payload);
    await dmChannel.send({ content: `https://discord.gg/${invite.code}` });
  }
}

module.exports = {
  UserHandler,
};
