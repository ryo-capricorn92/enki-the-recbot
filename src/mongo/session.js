/**
 * @typedef {import('./index.js').Mongo} Mongo
 */

class Session {
  #db = process.env.ATLAS_DB;

  /**
   * @param {Mongo} mongo
   */
  constructor(mongo) {
    this.mongo = mongo;
    this.client = mongo.client;
    this.collection = 'session';
  }

  async addWorkToRead(sessionId, workId) {
    const filter = { _id: sessionId };
    const update = { $push: { read: workId } };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update);
  }

  async addWorkToIgnored(sessionId, workId) {
    const filter = { _id: sessionId };
    const update = { $push: { ignored: workId } };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update);
  }

  async createSession(userId, sessionName = 'default') {
    const session = {
      user: userId,
      name: sessionName,
      read: [],
      ignored: [],
    };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.insertOne(session);
  }

  async findOrCreateFallback(userId, sessionId) {
    const sessions = await this.getSessionsByUser(userId);
    const found = sessions.find((s) => !s._id.equals(sessionId));
    let fallbackId = (found || {})._id;

    if (!found) {
      const { insertedId } = await this.createSession(userId);
      fallbackId = insertedId;
    }

    return fallbackId;
  }

  async getSessionById(sessionId) {
    const filter = { _id: sessionId };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.findOne(filter);
  }

  async getSessionsByUser(userId) {
    const filter = { user: userId };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.find(filter).toArray();
  }

  async deleteSession(sessionId) {
    const filter = { _id: sessionId };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.deleteOne(filter);
  }

  async resetReadWorks(sessionId) {
    const filter = { _id: sessionId };
    const update = { $set: { read: [] } };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update);
  }

  async resetIgnoredWorks(sessionId) {
    const filter = { _id: sessionId };
    const update = { $set: { ignored: [] } };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update);
  }
}

module.exports = Session;
