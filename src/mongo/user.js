/**
 * @typedef {import('./index.js').Mongo} Mongo
 */

class User {
  #db = process.env.ATLAS_DB;

  /**
   * @param {Mongo} mongo
   */
  constructor(mongo) {
    this.mongo = mongo;
    this.client = mongo.client;
    this.collection = 'user';
  }

  async getUserById(userId) {
    const filter = { _id: userId };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.findOne(filter);
  }

  async setActiveSession(userId, sessionId) {
    console.log({ userId, sessionId });
    const filter = { _id: userId };
    const update = { $set: { activeSession: sessionId } };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update);
  }

  async upsertUser(user) {
    const filter = { _id: user._id };
    const update = { $set: user };
    const options = { upsert: true };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update, options);
  }
}

module.exports = User;
