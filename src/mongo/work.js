const { EnkiError, TYPES } = require('../helpers/error');
const { calculateScore, getWorkId } = require('../util');

const getAverageStat = (works, field) => works
  .reduce((count, work) => count + work.stats[field], 0) / (works.length || 1);

const groupWorksByTrackedFandom = (works) => {
  const grouped = {};
  const ungrouped = [];

  works.forEach((work) => {
    if (!work.scoredBy) {
      ungrouped.push(work);
    } else {
      if (!grouped[work.scoredBy]) {
        grouped[work.scoredBy] = {
          tag: work.scoredBy,
          works: [],
        };
      }

      grouped[work.scoredBy].works.push(work);
    }
  });

  const groups = Object.values(grouped);

  if (ungrouped.length) {
    groups.push({
      tag: '',
      works: ungrouped,
    });
  }

  return groups;
};

/**
 * @typedef {import('./index.js').Mongo} Mongo
 */

class Work {
  #db = process.env.ATLAS_DB;

  /**
   * @param {Mongo} mongo
   */
  constructor(mongo) {
    this.mongo = mongo;
    this.client = mongo.client;
    this.collection = 'work';
  }

  async findAveragesForTag(decodedTag) {
    if (!decodedTag) { return null; }
    const topBookmarks = await this.mongo.work.getTopWorksByField(decodedTag, 'stats.bookmarks');
    const topKudos = await this.mongo.work.getTopWorksByField(decodedTag, 'stats.kudos');

    return {
      bookmarks: getAverageStat(topBookmarks, 'bookmarks'),
      kudos: getAverageStat(topKudos, 'kudos'),
    };
  }

  /**
   * @param {Object} options
   * @param {string} options.ratingsRange
   * @param {?string} options.tag
   * @param {?string[]} options.additionalTags
   * @param {?string[]} options.requireWarnings
   * @param {?string[]} options.hideWarnings
   * @param {Object} session
   * @param {string[]} session.read
   */
  async getRandomWorkByScore(options, session) {
    // create a query based off the user inputs
    let match = {
      _id: { $nin: [...session.read, ...session.ignored] },
      rating: { $in: options.ratingsRange },
    };

    // add main tag to query
    if (options.tag) {
      const mains = await this.mongo.tag.findTagsByTags([options.tag], { caseInsensitive: true });
      if (!mains.length) {
        throw new EnkiError(TYPES.MAIN_TAG_UNTRACKED, { tag: options.tag });
      }

      match = {
        ...match,
        trackedTags: {
          $elemMatch: {
            master: {
              $in: mains.map(({ master }) => master),
            },
          },
        },
      };
    }

    // add warnings to query
    if (options.requireWarnings.length && !options.hideWarnings.length) {
      match = {
        ...match,
        warnings: {
          $in: options.requireWarnings,
        },
      };
    } else if (options.hideWarnings.length) {
      match = {
        ...match,
        warnings: {
          $nin: options.hideWarnings,
        },
      };

      if (options.requireWarnings.length) {
        match = {
          $and: [
            match,
            {
              warnings: {
                $in: options.requireWarnings,
              },
            },
          ],
        };
      }
    }

    // add additional tags to query
    if (options.additionalTags) {
      const additionals = await this.mongo.tag
        .findTagsByTags(options.additionalTags, { caseInsensitive: true });

      if (match.$and) {
        match.$and.push({
          trackedTags: {
            $elemMatch: {
              master: {
                $in: additionals.map(({ master }) => master),
              },
            },
          },
        });
      } else {
        match = {
          $and: [
            match,
            {
              trackedTags: {
                $elemMatch: {
                  master: {
                    $in: additionals.map(({ master }) => master),
                  },
                },
              },
            },
          ],
        };
      }
    }

    // get 20 random works that match the search
    const col = await this.client.db(this.#db).collection(this.collection);
    const works = await col.aggregate([
      { $match: match },
      { $sample: { size: 20 } },
    ]).toArray();

    // handle empty results lists
    if (!works.length) {
      throw new EnkiError(TYPES.NO_WORKS_FOUND);
    }

    // pick the work with the highest score in the group
    const best = works.reduce((prev, cur) => ((prev.score > cur.score) ? prev : cur));

    return best;
  }

  async getTopWorksByField(tag, field, count = 20) {
    const query = { trackedTags: tag };

    const col = await this.client.db(this.#db).collection(this.collection);
    const works = await col
      .find(query)
      .sort({ [field]: -1 })
      .limit(count);

    return works.toArray();
  }

  async getWork(workId) {
    const filter = { _id: workId };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.findOne(filter);
  }

  async ingestWorks({ works, pageCount }, tagMaster) {
    // update tag's page count
    await this.mongo.tag.updatePageCount(tagMaster, pageCount || 1);

    // add tracked tags to works
    const worksWithTracked = await Promise.all(works.map(async (work) => {
      const trackedTags = await this.mongo.tag.findTrackedTags(work);

      return {
        ...work,
        trackedTags,
      };
    }));

    // now we can get averages by primary fandoms
    const grouped = groupWorksByTrackedFandom(worksWithTracked);
    const scoredWorks = grouped.map((group) => {
      const averages = this.findAveragesForTag(group.tag);

      // calculate scores for all works
      return group.works.map((work) => {
        const score = calculateScore(work, averages);

        return {
          ...work,
          score,
        };
      });
    }).flat();

    // finally update or add the works with their scores
    await this.mongo.work.upsertWorks(scoredWorks);

    return scoredWorks;
  }

  async updateTrackedTags(tag) {
    const tags = [tag.master, ...tag.synonyms];
    const query = {
      trackedTags: {
        $elemMatch: {
          master: { $ne: tag.master },
        },
      },
      $or: [
        { fandoms: { $in: tags } },
        { relationships: { $in: tags } },
        { tags: { $in: tags } },
      ],
    };

    const col = await this.client.db(this.#db).collection(this.collection);
    const found = await col.find(query).toArray();

    const updates = found.map((work) => {
      const synonym = tags.find((t) => t === tag.master || tag.synonyms.includes(t));

      return {
        updateOne: {
          filter: { _id: work._id },
          update: {
            $addToSet: {
              trackedTags: {
                master: tag.master,
                synonymUsed: synonym,
              },
            },
          },
        },
      };
    });

    if (updates.length) {
      await col.bulkWrite(updates);
    }
  }

  async upsertWork(work) {
    if (!work) { return null; }

    const id = getWorkId(work.link);

    if (!id) {
      throw new Error(`Unable to get ID for work ${work.link}`);
    }

    const filter = { _id: id };
    const update = { $set: work };
    const options = { upsert: true };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update, options);
  }

  async upsertWorks(works) {
    if (!works.length) { return; }

    const updates = await Promise.all(works
      .map(async (work) => {
        const id = getWorkId(work.link);

        if (!id) {
          console.log(`Unable to get ID for work ${work.link}. Skipping . . .`);
          return null;
        }

        return {
          updateOne: {
            filter: { _id: id },
            update: { $set: work },
            upsert: true,
          },
        };
      })
      .filter((update) => !!update));

    const col = await this.client.db(this.#db).collection(this.collection);
    await col.bulkWrite(updates);
  }
}

module.exports = Work;
