const { MongoClient } = require('mongodb');

const Session = require('./session');
const Tag = require('./tag');
const User = require('./user');
const Work = require('./work');

class Mongo {
  #uri = `${process.env.ATLAS_HOST}?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority`;

  #cert = (process.env.ATLAS_CERT || '').replace(/\\n/g, '\n');

  #db = process.env.ATLAS_DB;

  async init() {
    this.client = new MongoClient(this.#uri, {
      cert: this.#cert,
      key: this.#cert,
      minPoolSize: 5,
      maxPoolSize: 20,
    });

    await this.client.connect();
    await this.client.db(this.#db); // confirm we can fetch a DB

    this.session = new Session(this);
    this.tag = new Tag(this);
    this.user = new User(this);
    this.work = new Work(this);
  }
}

module.exports = {
  Mongo,
};
