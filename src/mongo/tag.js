/**
 * @typedef {import('./index.js').Mongo} Mongo
 */

const { makeRegexFriendly } = require('../util');

class Tag {
  #db = process.env.ATLAS_DB;

  /**
   * @param {Mongo} mongo
   */
  constructor(mongo) {
    this.mongo = mongo;
    this.client = mongo.client;
    this.collection = 'tag';
  }

  /**
   * @param {string[]} tags
   * @param {Object} options
   * @param {boolean} options.caseInsensitive
   * @param {boolean} options.includeUncommon
   */
  async findTagsByTags(tags, options = {}) {
    let tagsSearch = { $in: tags };

    if (options.caseInsensitive) {
      const regexTags = tags.map((tag) => makeRegexFriendly(tag));
      const appendi = ' - Fandom| - Freeform| - Relationship';
      const combined = new RegExp(`^(${regexTags.join('|')})(${appendi})*$`, 'i');
      tagsSearch = { $regex: combined };
    }

    const query = {
      isCommon: !options.includeUncommon,
      $or: [
        { master: tagsSearch },
        { synonyms: tagsSearch },
      ],
    };

    const col = await this.client.db(this.#db).collection(this.collection);
    const found = await col.find(query).toArray();

    return found;
  }

  async findTrackedTags(work) {
    const tags = [
      ...work.fandoms,
      ...work.relationships,
      ...work.tags,
    ];

    const query = {
      isCommon: true,
      $or: [
        { master: { $in: tags } },
        { synonyms: { $in: tags } },
      ],
    };

    const col = await this.client.db(this.#db).collection(this.collection);
    const found = await col.find(query).toArray();

    const ids = tags
      .map((tag) => found.find((t) => t.master === tag || t.synonyms.includes(tag))?._id)
      .filter((tag) => !!tag);

    // dedup
    return Array.from(new Set(ids));
  }

  async updatePageCount(master, pageCount) {
    const filter = { master };
    const update = { $set: { pageCount } };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update);
  }

  async upsertTag(tag) {
    const filter = { master: tag.master };
    const update = { $set: tag };
    const options = { upsert: true };

    const col = await this.client.db(this.#db).collection(this.collection);
    return col.updateOne(filter, update, options);
  }
}

module.exports = Tag;
