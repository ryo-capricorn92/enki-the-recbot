const { SlashCommandBuilder } = require('discord.js');

const { SessionHandler } = require('../handlers/session');

const NAME = 'session';

const payload = new SlashCommandBuilder()
  .setName(NAME)
  .setDescription('Shows details about the current session and provides management options');

const handler = (interaction, app) => new SessionHandler(interaction, app).handle();

module.exports = {
  handler,
  default: {
    name: NAME,
    payload,
    handler,
  },
};
