const { SlashCommandBuilder } = require('discord.js');

const { RATINGS } = require('../constants');
const { RecHandler } = require('../handlers/rec');

const NAME = 'rec';

const payload = new SlashCommandBuilder()
  .setName(NAME)
  .setDescription('Recommends a fanfic from AO3')
  .addStringOption(
    (option) => option
      .setName('tag')
      .setDescription('The fandom, pairing, or additional tag you want to search under - see `/help tags`'),
  )
  .addStringOption(
    (option) => option
      .setName('extratags')
      .setDescription('Any additional tags to search by - see `/help tags`'),
  )
  .addStringOption(
    (option) => option
      .setName('rating')
      .setDescription('The only rating you want to see - see `/help ratings`')
      .addChoices(...RATINGS.map((r) => ({ name: r, value: r }))),
  )
  .addStringOption(
    (option) => option
      .setName('minrating')
      .setDescription('The minimum rating you want to see - see `/help ratings`')
      .addChoices(...RATINGS.map((r) => ({ name: r, value: r }))),
  )
  .addStringOption(
    (option) => option
      .setName('maxrating')
      .setDescription('The maximum rating you want to see - see `/help ratings`')
      .addChoices(...RATINGS.map((r) => ({ name: r, value: r }))),
  )
  .addStringOption(
    (option) => option
      .setName('warningoptout')
      .setDescription('Show/Hide fics with \'Author Chose Not To Use Archive Warnings\' warning - see `/help warnings`')
      .addChoices(
        { name: 'Hide', value: 'hide' },
        { name: 'Show', value: 'show' },
        { name: 'Require', value: 'require' },
      ),
  )
  .addStringOption(
    (option) => option
      .setName('warningviolence')
      .setDescription('Show/Hide fics with \'Graphic Depictions Of Violence\' warning - see `/help warnings`')
      .addChoices(
        { name: 'Hide', value: 'hide' },
        { name: 'Show', value: 'show' },
        { name: 'Require', value: 'require' },
      ),
  )
  .addStringOption(
    (option) => option
      .setName('warningdeath')
      .setDescription('Show/Hide fics with \'Major Character Death\' warning - see `/help warnings`')
      .addChoices(
        { name: 'Hide', value: 'hide' },
        { name: 'Show', value: 'show' },
        { name: 'Require', value: 'require' },
      ),
  )
  .addStringOption(
    (option) => option
      .setName('warningnoncon')
      .setDescription('Show/Hide fics with \'Rape/Non-Con\' warning - see `/help warnings`')
      .addChoices(
        { name: 'Hide', value: 'hide' },
        { name: 'Show', value: 'show' },
        { name: 'Require', value: 'require' },
      ),
  )
  .addStringOption(
    (option) => option
      .setName('warningunderage')
      .setDescription('Show/Hide fics with \'Underage\' warning - see `/help warnings`')
      .addChoices(
        { name: 'Hide', value: 'hide' },
        { name: 'Show', value: 'show' },
        { name: 'Require', value: 'require' },
      ),
  );

const handler = (interaction, app) => new RecHandler(interaction, app).handle();

module.exports = {
  handler,
  default: {
    name: NAME,
    payload,
    handler,
  },
};
