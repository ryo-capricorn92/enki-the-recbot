const { SlashCommandBuilder } = require('discord.js');

const { ShowHandler } = require('../handlers/show');

const NAME = 'show';

const payload = new SlashCommandBuilder()
  .setName(NAME)
  .setDescription('Show an AO3 fic in the recommendation format')
  .addStringOption(
    (option) => option
      .setName('url')
      .setDescription('The URL for the AO3 work you want to display')
      .setRequired(true),
  )
  .addBooleanOption(
    (option) => option
      .setName('getfresh')
      .setDescription('Whether we should get the most recent info for the fic - will take a little longer'),
  );

const handler = (interaction, app) => new ShowHandler(interaction, app).handle();

module.exports = {
  handler,
  default: {
    name: NAME,
    payload,
    handler,
  },
};
