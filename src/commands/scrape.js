const { SlashCommandBuilder } = require('discord.js');

const { ScrapeHandler } = require('../handlers/scrape');

const NAME = 'scrape';

const payload = new SlashCommandBuilder()
  .setName(NAME)
  .setDescription('Scrapes the given url')
  .addStringOption((option) => option.setName('type')
    .setRequired(true)
    .setDescription('The type of scrape you want to do')
    .addChoices(
      { name: 'Initiate tag', value: 'init' },
      { name: 'Sample tagged works', value: 'single' },
      { name: 'Random tagged works', value: 'random' },
      { name: 'All tagged works', value: 'full' },
      { name: 'Tag details', value: 'tag' },
    ))
  .addStringOption((option) => option.setName('url')
    .setRequired(true)
    .setDescription('The URL you want to scrape'));

const handler = (interaction, app) => new ScrapeHandler(interaction, app).handle();

module.exports = {
  handler,
  default: {
    name: NAME,
    payload,
    handler,
  },
};
