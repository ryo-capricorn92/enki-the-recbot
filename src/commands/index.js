const { REST } = require('discord.js');
const { Collection, Routes } = require('discord.js');

const recCom = require('./rec').default;
const scrapeCom = require('./scrape').default;
const sessionCom = require('./session').default;
const showCom = require('./show').default;

const commandList = new Collection([
  [recCom.name, recCom],
  [scrapeCom.name, scrapeCom],
  [sessionCom.name, sessionCom],
  [showCom.name, showCom],
]);

const addCommands = async (rest) => {
  const commands = commandList.map((com) => com.payload);

  const route = Routes.applicationCommands(process.env.DISCORD_CLIENT_ID);

  await rest.put(route, { body: commands });
  console.log('Registered commands!');
};

const initCommands = async () => {
  const rest = new REST({ version: '10' }).setToken(process.env.DISCORD_TOKEN);

  // await deleteCommands(rest);
  await addCommands(rest);
};

module.exports = {
  commandList,
  initCommands,
};
