const WARNING_SHORTHANDS = {
  warningoptout: '???',
  warningnone: 'None',
  warningviolence: 'Violence',
  warningdeath: 'Death',
  warningnoncon: 'Rape',
  warningunderage: 'Underage',
};

const FIC_WARNINGS = {
  'Creator Chose Not To Use Archive Warnings': WARNING_SHORTHANDS.warningoptout,
  'No Archive Warnings Apply': WARNING_SHORTHANDS.warningnone,
  'Graphic Depictions Of Violence': WARNING_SHORTHANDS.warningviolence,
  'Rape/Non-Con': WARNING_SHORTHANDS.warningnoncon,
  'Major Character Death': WARNING_SHORTHANDS.warningdeath,
  Underage: WARNING_SHORTHANDS.warningunderage,
};

const RATINGS = [
  'General Audiences',
  'Teen And Up Audiences',
  'Mature',
  'Explicit',
  'No Rating',
];

const RATING_COLOR = {
  'General Audiences': '#8FC426',
  'Teen And Up Audiences': '#F1DF3F',
  Mature: '#EF8932',
  Explicit: '#A70A12',
  'No Rating': '#DADADA',
};

module.exports = {
  WARNING_SHORTHANDS,
  FIC_WARNINGS,
  RATINGS,
  RATING_COLOR,
};
