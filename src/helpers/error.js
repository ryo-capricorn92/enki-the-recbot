const { EmbedBuilder } = require('discord.js');

const { getRelativeTime } = require('../util');

const TYPES = {
  MAIN_TAG_UNTRACKED: 'MAIN_TAG_UNTRACKED',
  NO_WORKS_FOUND: 'NO_WORKS_FOUND',
  WORK_NOT_FOUND: 'WORK_NOT_FOUND',
  LOCKED_WORK: 'LOCKED_WORK',
  THROTTLED: 'THROTTLED',
  GENERIC: 'GENERIC',
};

class EnkiError extends Error {
  constructor(type, details = {}) {
    super();
    this.isEnkiError = !!TYPES[type];
    this.type = type;
    this.details = details;
    this.payload = this.getMessageForType();
  }

  getMessageForType() {
    const generic = new EmbedBuilder()
      .setDescription(`
Woops! Looks like we ran into an issue processing your request. Please open a ticket through the support server, or with the \`/ticket\` command.
      `);

    switch (this.type) {
      case TYPES.MAIN_TAG_UNTRACKED:
        return this.mainTagUntracked();
      case TYPES.NO_WORKS_FOUND:
        return this.noWorksFound();
      case TYPES.WORK_NOT_FOUND:
        return this.workNotFound();
      case TYPES.LOCKED_WORK:
        return this.workLocked();
      case TYPES.THROTTLED:
        return this.throttled();
      case TYPES.GENERIC:
      default:
        return { isEmbed: true, embed: generic };
    }
  }

  mainTagUntracked() {
    const message = `
Woops! Looks like **${this.details?.tag}** isn't tracked by EnkiBot yet. Currently, EnkiBot can only search the tags it knows.

Want to have this tag added to EnkiBot's tracked tags? You can request it using the \`/reqest\` command. See \`/help request\` for more details.
    `;

    const embed = new EmbedBuilder()
      .setDescription(message);

    return {
      isEmbed: true,
      embed,
    };
  }

  noWorksFound() { // eslint-disable-line class-methods-use-this
    const message = `
Woops! Looks like we couldn't find any works under that search. Your search might be a unicorn (surely no one's ever written a Teen-rated fluffy dad fic about your favorite tired dad, right?) or you might have marked them all as read!

You can always try out a new session if you'd like to see stuff you've read before - see \`/help session\` for more details.
    `;

    const embed = new EmbedBuilder()
      .setDescription(message);

    return {
      isEmbed: true,
      embed,
    };
  }

  workNotFound() { // eslint-disable-line class-methods-use-this
    const message = `
Woops! Looks like we couldn't find that fic. Make sure your URL is formatted properly - try popping it in a new tab exactly as you pasted it here and see if you get the fic you're looking for.

For futher help please open a ticket through the support server, or with the \`/ticket\` command.
    `;

    const embed = new EmbedBuilder()
      .setDescription(message);

    return {
      isEmbed: true,
      embed,
    };
  }

  workLocked() { // eslint-disable-line class-methods-use-this
    const message = `
Woops! Looks like you're trying to access a locked work. These locks require users to be logged in and are meant to prevent automated access by design. To respect the wishes of these authors, Enki will not attempt to interact with locked fics.

If you would still like to display a formatted link to the locked fic, check out the \`/manual\` command.
    `;

    const embed = new EmbedBuilder()
      .setDescription(message);

    return {
      isEmbed: true,
      embed,
    };
  }

  throttled() {
    const message = `
Woops! Looks like we're experiencing higher traffic than normal. Since we don't want to overload AO3 with requests from our bot, we need to wait a bit before we ask them for any more information.

You can try again ${getRelativeTime(this.details.timeout)}. In the meantime, you can still use the \`/rec\` command to get existing info Enki has asked for in the past. See \`/help throttling\` for more details.
    `;

    const embed = new EmbedBuilder()
      .setDescription(message);

    return {
      isEmbed: true,
      embed,
    };
  }
}

module.exports = {
  EnkiError,
  TYPES,
};
