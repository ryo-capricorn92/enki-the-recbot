const {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  EmbedBuilder,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
  StringSelectMenuBuilder,
  hyperlink,
  userMention,
  StringSelectMenuOptionBuilder,
} = require('discord.js');

const { EnkiError, TYPES } = require('./error');
const { RATING_COLOR } = require('../constants');
const { truncate } = require('../util');

class DiscordDisplay {
  constructor(payload) {
    this.payload = {
      content: undefined,
      embeds: [],
      components: [],
      ephemeral: false,
      ...(payload || {}),
    };
  }

  getPayload() {
    const { content, embeds, components } = this.payload;
    if (!content && !embeds.length && !components.length) {
      throw new EnkiError(TYPES.GENERIC, { error: 'Discord display message must contain either content, an embed, or a component.' });
    }

    return this.payload;
  }

  /* Works */
  addRec(work) {
    const authors = work.authors.join(', ');
    const warnings = work.warnings.join(',') || 'None';
    const fandoms = work.fandoms.map((t) => `__${t}__`).join(', ') || 'None';
    const relationships = work.relationships.map((t) => `__${t}__`).join(', ') || 'None';
    const otherTags = work.tags.map((t) => `__${t}__`).join(', ') || 'None';

    /* eslint-disable newline-per-chained-call */
    const authorTS = new Date(work.authorUpdatedAt).toUTCString().split(' ').slice(1, 4).join(' ');
    const enkiTS = new Date(work.enkiUpdatedAt).toUTCString().split(' ').slice(1, 4).join(' ');
    /* eslint-enable newline-per-chained-call */
    const footer = `Author last updated on ${authorTS} - | - Enki last updated on ${enkiTS}`;

    const fields = [
      { name: 'Rating', value: work.rating, inline: true },
      { name: '\u200b', value: '\u200b', inline: true },
      { name: 'Chapters', value: work.chapters, inline: true },
      { name: 'Warnings', value: warnings, inline: true },
      { name: '\u200b', value: '\u200b', inline: true },
      { name: 'Words', value: work.stats.wordCount.toLocaleString(), inline: true },
      { name: 'Fandoms', value: truncate(fandoms, 1024, '...__') },
      { name: 'Relationship(s)', value: truncate(relationships, 1024, '...__') },
      { name: 'Tags', value: truncate(otherTags, 1024, '...__') },
      { name: 'Summary', value: truncate(work.summary, 1024, '...') },
    ];

    const filteredFields = this.filterFields(fields);

    try {
      const embed = new EmbedBuilder()
        .setTitle(work.title)
        .setURL(work.link)
        .setAuthor({ name: authors })
        .setColor(RATING_COLOR[work.rating.trim()])
        .addFields(filteredFields)
        .setFooter({ text: footer });

      this.payload.embeds.push(embed);
    } catch (e) {
      console.info(e);
      throw e;
    }

    return this;
  }

  addWorkOptions() {
    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('markAsRead')
          .setLabel('Mark as read')
          .setStyle(ButtonStyle.Secondary),
        new ButtonBuilder()
          .setCustomId('markAsIgnored')
          .setLabel('Ignore')
          .setStyle(ButtonStyle.Secondary),
        new ButtonBuilder()
          .setCustomId('addToList')
          .setLabel('Add to list')
          .setStyle(ButtonStyle.Secondary),
      );

    this.payload.components.push(row);

    return this;
  }

  /* Sessions */

  addSessionDetails(session, userId) {
    const embed = new EmbedBuilder()
      .setTitle('Current Session')
      .addFields([
        { name: 'Session Name', value: session.name, inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Session Owner', value: userMention(userId), inline: true },
        { name: 'Read Works', value: `${session.read?.length || 0}`, inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Ignored Works', value: `${session.ignored?.length || 0}`, inline: true },
      ]);

    this.payload.embeds.push(embed);

    return this;
  }

  addManageSessionButtons() {
    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('addNewSession')
          .setLabel('Add New Session')
          .setStyle(ButtonStyle.Secondary),
      )
      .addComponents(
        new ButtonBuilder()
          .setCustomId('changeSession')
          .setLabel('Change Session')
          .setStyle(ButtonStyle.Secondary),
      )
      .addComponents(
        new ButtonBuilder()
          .setCustomId('deleteSession')
          .setLabel('Delete Current Session')
          .setStyle(ButtonStyle.Secondary),
      );

    this.payload.components.push(row);

    return this;
  }

  addPruneSessionButtons() {
    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('resetRead')
          .setLabel('Reset Read Works')
          .setStyle(ButtonStyle.Secondary),
      )
      .addComponents(
        new ButtonBuilder()
          .setCustomId('resetIgnored')
          .setLabel('Reset Ignored Works')
          .setStyle(ButtonStyle.Secondary),
      );

    this.payload.components.push(row);

    return this;
  }

  addChangeSession(sessions, currentId) {
    const options = sessions.map((session) => new StringSelectMenuOptionBuilder()
      .setLabel(session.name)
      .setDescription(`${session.read?.length || 0} read | ${session.ignored?.length || 0} ignored`)
      .setValue(session._id.toString())
      .setDefault(session._id.equals(currentId)));

    const select = new StringSelectMenuBuilder()
      .setCustomId('setSession')
      .setPlaceholder('Select a session to change to:')
      .addOptions(options);

    const cancelButton = new ButtonBuilder()
      .setCustomId('cancelSessionChange')
      .setLabel('Cancel')
      .setStyle(ButtonStyle.Secondary);

    const row1 = new ActionRowBuilder().addComponents(select);
    const row2 = new ActionRowBuilder().addComponents(cancelButton);

    this.payload.components.push(row1, row2);

    return this;
  }

  addDeleteSession() {
    const embed = new EmbedBuilder()
      .setDescription('Are you sure you want to delete the current session?');

    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('confirmSessionDelete')
          .setLabel('Delete')
          .setStyle(ButtonStyle.Danger),
        new ButtonBuilder()
          .setCustomId('cancelSessionDelete')
          .setLabel('Cancel')
          .setStyle(ButtonStyle.Secondary),
      );

    this.payload.embeds.push(embed);
    this.payload.components.push(row);

    return this;
  }

  addResetRead() {
    const embed = new EmbedBuilder()
      .setDescription('Are you sure you want to reset the read works for your current session? Works marked as read do not show up in rec searches.');

    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('confirmReadReset')
          .setLabel('Reset')
          .setStyle(ButtonStyle.Danger),
        new ButtonBuilder()
          .setCustomId('cancelReadReset')
          .setLabel('Cancel')
          .setStyle(ButtonStyle.Secondary),
      );

    this.payload.embeds.push(embed);
    this.payload.components.push(row);

    return this;
  }

  addResetIgnored() {
    const embed = new EmbedBuilder()
      .setDescription('Are you sure you want to reset the ignored works for your current session? Works marked as ignored do not show up in rec searches.');

    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('confirmIgnoredReset')
          .setLabel('Reset')
          .setStyle(ButtonStyle.Danger),
        new ButtonBuilder()
          .setCustomId('cancelIgnoredReset')
          .setLabel('Cancel')
          .setStyle(ButtonStyle.Secondary),
      );

    this.payload.embeds.push(embed);
    this.payload.components.push(row);

    return this;
  }

  /* User */
  addWelcomeMessage() {
    const embed = new EmbedBuilder()
      .setTitle('Welcome to Enki!')
      .setFooter({ text: `Version ${process.env.npm_package_version}` })
      .setDescription(`
  *Enki is currently in Alpha development. This means many features may be incomplete, or contain bugs.*

  Looks like this is your first time using Enki - we're excited to have you! Remember to check out the ${hyperlink('documentation', 'https://gitlab.com/ryo-capricorn92/enki-the-recbot')} or use the \`/help\` command to learn more about how to use Enki. You can even try it out right in these DMs!

  If you haven't yet, be sure to join the support server for updates, surveys, and help with all your Enki needs.
      `);

    this.payload.embeds.push(embed);

    return this;
  }

  /* Modals */

  /* eslint-disable class-methods-use-this */
  generateNewSessionModal(name) {
    const modal = new ModalBuilder()
      .setCustomId('newSession')
      .setTitle('Create New Session');

    const nameInput = new TextInputBuilder()
      .setCustomId('sessionName')
      .setLabel('Session Name')
      .setMinLength(1)
      .setMaxLength(31)
      .setStyle(TextInputStyle.Short)
      .setPlaceholder(name)
      .setValue(name)
      .setRequired(true);

    const row = new ActionRowBuilder().addComponents(nameInput);
    modal.addComponents(row);

    return modal;
  }

  /* Other */

  makeStatic() {
    const payload = { components: [] };
    const text = 'This message has expired';

    if (this.payload.embeds.length) {
      const [primary] = this.payload.embeds;
      const staticEmbed = new EmbedBuilder(primary)
        .setFooter({ text })
        .setTimestamp();

      payload.embeds = [staticEmbed];
    } else {
      payload.content = text;
    }

    this.payload = payload;

    return this;
  }

  filterFields(fields) {
    // TODO: filter fields and inline settings by user/server preferences
    return fields;
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  DiscordDisplay,
};
