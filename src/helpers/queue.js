const EventEmitter = require('events');
const { v4 } = require('uuid');

const { TYPES } = require('./error');

class Queue {
  constructor(event) {
    if (!(event instanceof EventEmitter)) {
      throw new Error('Queue event must be an event emitter');
    }

    this.event = event;

    this.queue = {};
    this.head = 0;
    this.tail = 0;

    this.paused = false;
    this.running = false;
    this.runnerId = null;

    this.event.on('throttle', () => {
      // if a run is currently active, pausing will stop it
      this.paused = true;
    });

    this.event.on('unthrottle', () => {
      this.paused = false;

      // let's restart the runner
      if (!this.running) {
        this.run();
      }
    });
  }

  get length() {
    return this.tail - this.head;
  }

  enqueue(payload, handler) {
    this.queue[this.tail] = { payload, handler };
    this.tail += 1;

    if (!this.running && !this.paused) {
      this.run();
    }
  }

  dequeue() {
    const payload = this.queue[this.head];

    delete this.queue[this.head];
    this.head += 1;

    return payload;
  }

  async run() {
    // we're doing a run now
    this.running = true;

    // let's give this run an id
    const runnerId = v4();
    this.runnerId = runnerId;

    try {
      // let's keep trying to handle entries until:
      // 1) the runner id changes
      // 2) the queue is paused (ie, for throttling)
      // 3) the queue is empty
      while (this.runnerId === runnerId && !this.paused && this.length > 0) {
        const { handler, payload } = this.dequeue();
        await handler(payload); // eslint-disable-line no-await-in-loop
      }
    } catch (e) {
      // if the handler throws an error, we need to check for throttling
      if (e.isEnkiError && e.type === TYPES.THROTTLED) {
        this.event.emit('throttle', e);
      } else {
        console.error(e);
      }
    }

    if (this.runnerId === runnerId) {
      this.running = false;
    }
  }
}

module.exports = {
  Queue,
};
