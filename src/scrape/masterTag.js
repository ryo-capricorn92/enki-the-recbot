const getDescription = ($) => $('div.home')
  .children('p')
  .map(function findDescriptions() {
    return $(this).text();
  })
  .get()
  .join('');

const getMaster = ($) => $('div.primary')
  .children('h2.heading')
  .last()
  .text();

const getSynonyms = ($) => $('div.synonym')
  .find('ul.tags')
  .children('li')
  .map(function findSynonyms() {
    return $(this).text();
  })
  .get();

const getCategoryFromDescription = (description) => {
  const catMatch = description.match(/This tag belongs to the ([a-zA-Z\s]+) Category./);

  const category = (!catMatch || !catMatch[1]) ? 'Other' : catMatch[1];
  return category;
};

const getCommonFromDescription = (description) => {
  const isCommon = /It's a (common|canonical) tag./.test(description);
  return isCommon;
};

const extractTag = ($) => {
  const description = getDescription($);

  return {
    master: getMaster($),
    synonyms: getSynonyms($),
    category: getCategoryFromDescription(description),
    isCommon: getCommonFromDescription(description),
    pageCount: 1,
  };
};

module.exports = {
  extractTag,
};
