const axios = require('axios');
const cheerio = require('cheerio');

const { EnkiError, TYPES } = require('../helpers/error');
const { getTagUrl, getPage, getRandomInRange } = require('../util');

const masterTag = require('./masterTag');
const taggedWorks = require('./taggedWorks');
const workScrape = require('./work');

const options = {
  headers: { 'User-Agent': `EnkiBot/${process.env.npm_package_version} "AO3 scraping recommendation bot"` },
};

const checkForThrottle = (e) => {
  if (e instanceof EnkiError) { return e; }

  if (e.isAxiosError && e.response?.status === 429) {
    const timeoutSeconds = e.response.headers['retry-after'] || 1000 * 60 * 15; // default to 15 mins
    const timeout = Date.now() + (timeoutSeconds * 1000);

    return new EnkiError(TYPES.THROTTLED, { timeout });
  }

  if (e.isAxiosError && e.response?.status === 404) {
    return new EnkiError(TYPES.WORK_NOT_FOUND);
  }

  return e;
};

const scrapeMasterTag = async (url) => {
  const tagUrl = getTagUrl(url);
  if (!tagUrl) { throw new Error(`Unable to find base url for ${url}`); }

  try {
    const res = await axios.get(tagUrl, options);
    const $ = cheerio.load(res.data);
    const tag = masterTag.extractTag($);

    return {
      ...tag,
      link: tagUrl,
    };
  } catch (e) {
    throw checkForThrottle(e);
  }
};

const scrapeTaggedWorks = async (url) => {
  const isSearch = /^https:\/\/archiveofourown.org\/works\?/.test(url);
  const page = getPage(url);
  const worksUrl = isSearch ? url : `${getTagUrl(url)}/works${page ? `?page=${page}` : ''}`;

  try {
    const res = await axios.get(worksUrl, options);
    const $ = cheerio.load(res.data);

    const pageCount = taggedWorks.getPageCount($);
    const works = taggedWorks.extractWorks($);

    return {
      pageCount,
      works,
    };
  } catch (e) {
    throw checkForThrottle(e);
  }
};

const scrapeRandomWorks = async (tag) => {
  const page = getRandomInRange(1, tag.pageCount);
  const url = `${tag.link}?page=${page}`;

  return scrapeTaggedWorks(url);
};

const scrapeWork = async (url) => {
  try {
    const res = await axios.get(url, options);
    const $ = cheerio.load(res.data);

    if (workScrape.checkForLocked($)) {
      throw new EnkiError(TYPES.LOCKED_WORK);
    }

    const work = workScrape.extractWork($, url);

    return work;
  } catch (e) {
    throw checkForThrottle(e);
  }
};

module.exports = {
  scrapeMasterTag,
  scrapeRandomWorks,
  scrapeTaggedWorks,
  scrapeWork,
};
