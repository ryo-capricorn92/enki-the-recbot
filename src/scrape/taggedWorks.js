const { FIC_WARNINGS } = require('../constants');

const getAuthors = ($, node) => $(node)
  .children()
  .first()
  .children()
  .first()
  .children('a')
  .filter(function findAuthors() {
    return $(this).attr('rel') === 'author';
  })
  .map(function findAuthorNames() {
    return $(this).text().split('/').slice(-1);
  })
  .get();

const getFandomTags = ($, node) => $(node)
  .find('h5.fandoms')
  .children('a.tag')
  .map(function findRelationships() {
    return $(this).text();
  })
  .get();

const getLink = ($, node) => $(node)
  .children()
  .first()
  .children()
  .first()
  .children()
  .first()
  .attr('href');

const getOtherTags = ($, node) => $(node)
  .find('ul.tags')
  .children('li.freeforms')
  .map(function findFreeforms() {
    return $(this).text();
  })
  .get();

const getPageCount = ($) => {
  const pageCount = $('ol.pagination li:not(.next)')
    .last()
    .text();

  return parseInt(pageCount, 10) || 1;
};

const getRating = ($, node) => $(node)
  .find('ul.required-tags')
  .children()
  .first()
  .text()
  .trim();

const getRelationshipTags = ($, node) => $(node)
  .find('ul.tags')
  .children('li.relationships')
  .map(function findRelationships() {
    return $(this).text();
  })
  .get();

const getSummary = ($, node) => $(node)
  .find('blockquote.summary')
  .children('p')
  .map(function findSummaryParts() {
    return $(this).html()
      .replace(/&nbsp;/gm, ' ')
      .replace(/<(\/)?s>/gm, '~~')
      .replace(/<(\/)?(em|i)>/gm, '_')
      .replace(/<(\/)?(strong|b)>/gm, '**')
      .replace(/<(\/)?br>/gm, `
      `);
  })
  .get()
  .join(`

  `);

const getTitle = ($, node) => $(node)
  .children()
  .first()
  .children()
  .first()
  .children()
  .first()
  .text();

const getWarnings = ($, node) => $(node)
  .find('ul.tags')
  .children('li.warnings')
  .map(function findWarnings() {
    return $(this).text();
  })
  .get()
  .map((warning) => FIC_WARNINGS[warning]);

const getAuthorUpdateAt = ($, node) => $(node)
  .find('p.datetime')
  .text();

const getWordCount = ($, node) => {
  const words = $(node)
    .find('dd.words')
    .text();

  return parseInt(words.replace(/,/, ''), 10);
};

const getKudos = ($, node) => {
  const kudos = $(node)
    .find('dd.kudos')
    .text();

  return parseInt(kudos, 10) || 0;
};

const getBookmarks = ($, node) => {
  const bookmarks = $(node)
    .find('dd.bookmarks')
    .text();

  return parseInt(bookmarks, 10) || 0;
};

const getComments = ($, node) => {
  const comments = $(node)
    .find('dd.comments')
    .text();

  return parseInt(comments, 10) || 0;
};

const getChapters = ($, node) => $(node)
  .find('dd.chapters')
  .text();

const getActualChapters = ($, node) => {
  const chapters = getChapters($, node);
  const [finished] = chapters.split('/');

  return parseInt(finished, 10) || 1;
};

const getHits = ($, node) => {
  const hits = $(node)
    .find('dd.hits')
    .text();

  return parseInt(hits, 10) || 0;
};

const getStats = ($, node) => ({
  comments: getComments($, node),
  chapters: getActualChapters($, node),
  hits: getHits($, node),
  kudos: getKudos($, node),
  bookmarks: getBookmarks($, node),
  wordCount: getWordCount($, node),
});

const extractWork = ($, node) => ({
  authors: getAuthors($, node),
  chapters: getChapters($, node),
  fandoms: getFandomTags($, node),
  link: `https://archiveofourown.org${getLink($, node)}`,
  rating: getRating($, node),
  relationships: getRelationshipTags($, node),
  stats: getStats($, node),
  summary: getSummary($, node),
  tags: getOtherTags($, node),
  title: getTitle($, node),
  warnings: getWarnings($, node),
  authorUpdatedAt: new Date(getAuthorUpdateAt($, node)),
  enkiUpdatedAt: new Date(),
});

const extractWorks = ($) => {
  const works = [];
  $('li.work').each(function () { // eslint-disable-line
    works.push(extractWork($, this));
  });
  return works;
};

module.exports = {
  extractWork,
  extractWorks,
  getPageCount,
};
