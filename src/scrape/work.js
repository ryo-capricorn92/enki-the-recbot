const { FIC_WARNINGS } = require('../constants');

const getAuthors = ($) => $('h3.byline')
  .children('a')
  .filter(function findAuthors() {
    return $(this).attr('rel') === 'author';
  })
  .map(function findAuthorNames() {
    return $(this).text().split('/').slice(-1);
  })
  .get();

const getFandomTags = ($) => $('dd.fandom')
  .find('a.tag')
  .map(function findRelationships() {
    return $(this).text();
  })
  .get();

const getOtherTags = ($) => $('dd.freeform')
  .find('a.tag')
  .map(function findFreeforms() {
    return $(this).text();
  })
  .get();

const getRating = ($) => {
  const res = $('dd.rating')
    .find('a.tag')
    .first()
    .text()
    .trim();

  return res;
};

const getRelationshipTags = ($) => $('dd.relationship')
  .find('a.tag')
  .map(function findRelationships() {
    return $(this).text();
  })
  .get();

const getSummary = ($) => $('blockquote.userstuff')
  .first()
  .children('p')
  .map(function findSummaryParts() {
    return $(this).html()
      .replace(/&nbsp;/gm, ' ')
      .replace(/<(\/)?s>/gm, '~~')
      .replace(/<(\/)?(em|i)>/gm, '_')
      .replace(/<(\/)?(strong|b)>/gm, '**')
      .replace(/<(\/)?br>/gm, `
      `);
  })
  .get()
  .join(`

  `);

const getTitle = ($) => $('h2.title').text().trim();

const getWarnings = ($) => $('dd.warning')
  .find('a.tag')
  .map(function findWarnings() {
    return $(this).text();
  })
  .get()
  .map((warning) => FIC_WARNINGS[warning]);

const getPublishedAt = ($) => $('dd.published').text();
const getUpdatedAt = ($) => $('dd.status').text();

const getWordCount = ($) => {
  const wordCount = $('dd.words').text();

  return parseInt(wordCount.replace(/,/, ''), 10);
};

const getKudos = ($) => {
  const kudos = $('dd.kudos').text();

  return parseInt(kudos, 10) || 0;
};

const getBookmarks = ($) => {
  const bookmarks = $('dd.bookmarks').text();

  return parseInt(bookmarks, 10) || 0;
};

const getComments = ($) => {
  const comments = $('dd.comments').text();

  return parseInt(comments, 10) || 0;
};

const getChapters = ($) => $('dd.chapters').text();

const getActualChapters = ($) => {
  const chapters = getChapters($);
  const [finished] = chapters.split('/');

  return parseInt(finished, 10) || 1;
};

const getHits = ($) => {
  const hits = $('dd.hits').text();

  return parseInt(hits, 10) || 0;
};

const getStats = ($) => ({
  comments: getComments($),
  chapters: getActualChapters($),
  hits: getHits($),
  kudos: getKudos($),
  bookmarks: getBookmarks($),
  wordCount: getWordCount($),
});

const extractWork = ($, link) => ({
  authors: getAuthors($),
  chapters: getChapters($),
  fandoms: getFandomTags($),
  link,
  rating: getRating($),
  relationships: getRelationshipTags($),
  stats: getStats($),
  summary: getSummary($),
  tags: getOtherTags($),
  title: getTitle($),
  warnings: getWarnings($),
  authorUpdatedAt: new Date(getUpdatedAt($) || getPublishedAt($)),
  enkiUpdatedAt: new Date(),
});

const checkForLocked = ($) => {
  if ($('div#signin')) { return true; }
  return false;
};

module.exports = {
  checkForLocked,
  extractWork,
};
