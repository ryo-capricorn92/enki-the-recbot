const { Tag } = require('./tag');
const { Work } = require('./work');

const init = jest.fn();

class Mongo {
  constructor() {
    this.tag = new Tag(this);
    this.work = new Work(this);
    this.client = {};
  }

  async init(...args) {
    init(args);
    return this;
  }
}

module.exports = {
  Mongo,
  Tag,
  Work,
  init,
};
