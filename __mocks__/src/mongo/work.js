const findAveragesForTag = jest.fn();
const getRandomWorkByScore = jest.fn();
const getTopWorksByField = jest.fn();
const ingestWorks = jest.fn();
const updateTrackedTags = jest.fn();
const upsertWork = jest.fn();
const upsertWorks = jest.fn();

/* eslint-disable class-methods-use-this */
class Work {
  constructor(mongo) {
    this.mongo = mongo;
    this.client = mongo.client;
  }

  async findAveragesForTag(...args) {
    return findAveragesForTag(args);
  }

  async getRandomWorkByScore(...args) {
    return getRandomWorkByScore(args);
  }

  async getTopWorksByField(...args) {
    return getTopWorksByField(args);
  }

  async ingestWorks(...args) {
    return ingestWorks(args);
  }

  async updateTrackedTags(...args) {
    return updateTrackedTags(args);
  }

  async upsertWork(...args) {
    return upsertWork(args);
  }

  async upsertWorks(...args) {
    return upsertWorks(args);
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  Work,
  findAveragesForTag,
  getRandomWorkByScore,
  getTopWorksByField,
  ingestWorks,
  updateTrackedTags,
  upsertWork,
  upsertWorks,
};
