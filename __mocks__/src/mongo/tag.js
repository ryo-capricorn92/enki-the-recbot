const findTagsByTags = jest.fn();
const findTrackedTags = jest.fn();
const updatePageCount = jest.fn();
const upsertTag = jest.fn();

/* eslint-disable class-methods-use-this */
class Tag {
  constructor(mongo) {
    this.mongo = mongo;
    this.client = mongo.client;
  }

  async findTagsByTags(...args) {
    return findTagsByTags(args);
  }

  async findTrackedTags(...args) {
    return findTrackedTags(args);
  }

  async updatePageCount(...args) {
    return updatePageCount(args);
  }

  async upsertTag(...args) {
    return upsertTag(args);
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  Tag,
  findTagsByTags,
  findTrackedTags,
  updatePageCount,
  upsertTag,
};
