const scrapeMasterTag = jest.fn(async () => {});
const scrapeRandomWorks = jest.fn(async () => {});
const scrapeTaggedWorks = jest.fn(async () => {});
const scrapeWork = jest.fn(async () => {});

module.exports = {
  scrapeMasterTag,
  scrapeRandomWorks,
  scrapeTaggedWorks,
  scrapeWork,
};
