const { Message } = require('./Message');

const send = jest.fn();
const makeCode = jest.fn(() => ({ code: '123' }));

/* eslint-disable class-methods-use-this */
class Channel {
  async send(message) {
    send(message);
    return new Message();
  }

  async createInvite() {
    return makeCode();
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  Channel,
  send,
  makeCode,
};
