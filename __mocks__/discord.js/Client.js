const { Channel } = require('./Channel');

const channelCacheGet = jest.fn(async () => new Channel());

class Client {
  constructor() {
    this.channels = {
      cache: {
        get: channelCacheGet,
      },
    };
  }
}

module.exports = {
  Client,
  channelCacheGet,
};
