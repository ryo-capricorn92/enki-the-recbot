const { CommandInteraction } = require('./CommandInteraction');

module.exports = {
  ...jest.requireActual('discord.js'),
  CommandInteraction,
};
