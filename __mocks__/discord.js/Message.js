const { Collector } = require('./Collector');

class Message {
  constructor() {
    this.collector = new Collector();
  }

  createMessageComponentCollector() {
    return this.collector;
  }
}

module.exports = {
  Message,
};
