const { CommandInteractionOption } = require('./CommandInteractionOption');
const { Client } = require('./Client');
const { Message } = require('./Message');
const { User } = require('./User');

const deferReply = jest.fn();
const editReply = jest.fn();

/* eslint-disable class-methods-use-this */
class CommandInteraction {
  constructor(options, user) {
    this.options = new CommandInteractionOption(options);
    this.user = new User(user);
    this.client = new Client();
  }

  async deferReply(args) {
    deferReply(args);
    return new Message();
  }

  async editReply(args) {
    editReply(args);
    return new Message();
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  CommandInteraction,
  deferReply,
  editReply,
};
