class CommandInteractionOption {
  constructor(options) {
    this.givenOptions = options;
  }

  getBoolean(key) {
    return this.givenOptions[key];
  }

  getString(key) {
    return this.givenOptions[key];
  }
}

module.exports = {
  CommandInteractionOption,
};
