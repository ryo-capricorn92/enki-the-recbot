const EventEmitter = require('events');

/* eslint-disable no-underscore-dangle */
class Collector extends EventEmitter {
  constructor() {
    super();
    this._listenersForJest = {};
  }

  on(name, listener) {
    super.on(name, listener);
    this._listenersForJest[name] = listener;
  }

  emit(name, args) {
    super.emit(name, args);
    return this._listenersForJest[name](args);
  }
}
/* eslint-enable no-underscore-dangle */

module.exports = {
  Collector,
};
