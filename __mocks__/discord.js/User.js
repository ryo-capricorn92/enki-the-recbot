const { Channel } = require('./Channel');

/* eslint-disable class-methods-use-this */
class User {
  constructor(user = {}) {
    Object.keys(user).forEach((key) => {
      this[key] = user[key];
    });
  }

  async createDM() {
    return new Channel();
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  User,
};
