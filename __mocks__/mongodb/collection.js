const { Cursor } = require('./cursor');

const aggregate = jest.fn();
const bulkWrite = jest.fn();
const find = jest.fn();
const findOne = jest.fn();
const insertOne = jest.fn();
const updateOne = jest.fn();

/* eslint-disable class-methods-use-this */
class MongoCollection {
  aggregate(...args) {
    const result = aggregate(...args);
    return new Cursor(result);
  }

  async bulkWrite(...args) {
    return bulkWrite(...args);
  }

  find(...args) {
    const result = find(...args);
    return new Cursor(result);
  }

  async findOne(...args) {
    return findOne(...args);
  }

  async insertOne(...args) {
    return insertOne(...args);
  }

  async updateOne(...args) {
    return updateOne(...args);
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  MongoCollection,
  aggregate,
  bulkWrite,
  find,
  findOne,
  insertOne,
  updateOne,
};
