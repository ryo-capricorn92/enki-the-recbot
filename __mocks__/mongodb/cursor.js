class Cursor {
  constructor(result) {
    this.result = result;
  }

  limit() {
    return this;
  }

  sort() {
    return this;
  }

  toArray() {
    return this.result;
  }
}

module.exports = {
  Cursor,
};
