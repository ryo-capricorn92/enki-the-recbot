const { MongoCollection } = require('./collection');

/* eslint-disable class-methods-use-this */
class MongoClient {
  constructor(uri, options) {
    this.uri = uri;
    this.options = options;
  }

  async connect() {
    return this;
  }

  db() {
    return this;
  }

  async collection() {
    return new MongoCollection();
  }
}
/* eslint-enable class-methods-use-this */

module.exports = {
  MongoClient,
};
