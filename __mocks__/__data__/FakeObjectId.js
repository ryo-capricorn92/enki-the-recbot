class FakeObjectId {
  constructor(_id) {
    this._id = _id;
  }

  toString() {
    return `${this._id}`;
  }

  equals(_id) {
    return this._id === _id.toString();
  }
}

module.exports = FakeObjectId;
