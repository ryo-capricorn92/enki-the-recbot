const FakeObjectId = require('./FakeObjectId');

const TEXT_LONGER_THAN_1024 = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque consectetur sem id nisl congue varius. Etiam interdum lacinia mi sit amet aliquet. Sed eu rutrum eros, ac ultricies nibh. Proin sollicitudin volutpat purus ut fermentum. Nulla euismod ex et nulla tempor, egestas malesuada nisi luctus. Pellentesque dapibus ipsum eget risus dictum cursus. Ut blandit sollicitudin semper. Cras ut suscipit leo. Proin felis lorem, efficitur sit amet augue a, rhoncus pretium libero. In tempor erat a volutpat suscipit. Phasellus sollicitudin elit in justo mollis lacinia. Pellentesque a sapien eu nisl rutrum luctus. Integer rhoncus condimentum sem, sed pharetra neque rhoncus a. Vestibulum iaculis urna vitae dignissim eleifend. Sed ac metus lacus. Sed sed interdum risus. Integer sit amet ipsum quis mi pellentesque ullamcorper. Aliquam auctor sem neque, scelerisque facilisis lorem vestibulum eu. Phasellus ornare suscipit mauris non ornare. Cras pharetra imperdiet turpis, in feugiat metus eleifend eget. Nulla sodales neque placerat arcu faucibus vehicula. Proin bibendum nisl quis sapien tristique molestie. Integer vitae vulputate neque. Nam congue metus sed odio interdum luctus. Vivamus congue libero id justo vestibulum ultrices. Praesent posuere tincidunt velit, ac dictum massa mollis at. Nulla sed leo feugiat, rutrum erat sit amet, porta mauris. Duis finibus magna.';

const FANDOM_ID = new FakeObjectId('abc');
const RELATIONSHIP_ID = new FakeObjectId('cba');
const FREEFORM_ID_1 = new FakeObjectId('def');
const FREEFORM_ID_2 = new FakeObjectId('fed');
const SESSION_ID = new FakeObjectId('ghi');
const USER_ID = new FakeObjectId('321');
const WORK_ID = new FakeObjectId('123');

const FANDOM_TAG = {
  _id: FANDOM_ID,
  master: 'fandom1',
  link: 'https://archiveofourown.org/tags/fandom1',
  synonyms: [],
  category: 'Fandom',
  isCommon: true,
  pageCount: 30,
};

const RELATIONSHIP_TAG = {
  _id: RELATIONSHIP_ID,
  master: 'pairing/pairing 1',
  link: 'https://archiveofourown.org/tags/pairing*s*pairing%201',
  synonyms: ['guy/dude'],
  category: 'Relationship',
  isCommon: true,
  pageCount: 15,
};

const FREEFORM_TAG_1 = {
  _id: FREEFORM_ID_1,
  master: 'freeform1',
  link: 'https://archiveofourown.org/tags/fandom1',
  synonyms: ['synonym1'],
  category: 'Freeform',
  isCommon: true,
  pageCount: 10,
};

const FREEFORM_TAG_2 = {
  _id: FREEFORM_ID_2,
  master: 'freeform2',
  link: 'https://archiveofourown.org/tags/fandom2',
  synonyms: ['synonym2'],
  category: 'Freeform',
  isCommon: true,
  pageCount: 5,
};

const SESSION = {
  _id: SESSION_ID,
  user: USER_ID,
  name: 'default',
  read: [],
  ignored: [],
};

const USER = {
  _id: USER_ID,
  activeSession: SESSION_ID,
};

const WORK = {
  _id: WORK_ID,
  authors: ['writer'],
  chapters: '2/3',
  fandoms: [FANDOM_TAG.master],
  link: 'https://archiveofourown.org/works/123',
  rating: 'Mature',
  relationships: [
    RELATIONSHIP_TAG.synonyms[0],
  ],
  score: 12,
  stats: {
    comments: 10,
    chapters: 2,
    hits: 1000,
    kudos: 100,
    bookmarks: 10,
    wordCount: 5000,
  },
  summary: 'stuff happens',
  tags: [
    FREEFORM_TAG_1.master,
    FREEFORM_TAG_2.synonyms[0],
  ],
  title: 'cool stuff',
  trackedTags: [
    FANDOM_TAG._id,
    RELATIONSHIP_TAG._id,
    FREEFORM_TAG_2._id,
  ],
  warnings: ['Death'],
  authorUpdatedAt: '2024-11-05T00:00:00.000',
  enkiUpdatedAt: '2024-12-27T00:00:00.000',
};

module.exports = {
  TEXT_LONGER_THAN_1024,
  FANDOM_TAG,
  RELATIONSHIP_TAG,
  FREEFORM_TAG_1,
  FREEFORM_TAG_2,
  SESSION,
  USER,
  WORK,
  FakeObjectId,
};
