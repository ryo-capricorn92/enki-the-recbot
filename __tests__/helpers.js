const { setTimeout } = require('timers/promises');

class HandlerWithPauseButton {
  constructor(name) {
    this.name = name;
    this.paused = true;
    this.handle = jest.fn(async () => {
      while (this.paused) {
        await setTimeout(10); // eslint-disable-line no-await-in-loop
      }
    });
  }

  unpause() {
    this.paused = false;
  }
}

module.exports = {
  HandlerWithPauseButton,
};
