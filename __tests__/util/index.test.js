const {
  calculateScore,
  decodeAo3Url,
  getEncodedTag,
  getNextName,
  getPage,
  getRandomInRange,
  getRelativeTime,
  getTagUrl,
  getWorkId,
  makeRegexFriendly,
  truncate,
} = require('../../src/util');

const { WORK, TEXT_LONGER_THAN_1024 } = require('../../__mocks__/__data__');

describe('calculateScore()', () => {
  it('should return a number', () => {
    const score = calculateScore(WORK);
    expect(typeof score).toBe('number');
  });

  it('should properly calculate a score for a generic work without averages', () => {
    const score = calculateScore(WORK);
    const { stats: { bookmarks, kudos, comments, chapters } } = WORK;

    const bkr = (bookmarks / kudos) * 100;
    const ckr = ((comments / chapters) / kudos) * 100;

    const expectedScore = Math.round(bkr + ckr);
    expect(score).toBe(expectedScore);
  });

  it('should gracefully handle a generic work with 0 kudos and no averages', () => {
    const work = {
      ...WORK,
      stats: {
        ...WORK.stats,
        kudos: 0,
      },
    };
    const score = calculateScore(work);

    const { stats: { bookmarks, comments, chapters } } = work;
    const bkr = bookmarks * 100;
    const ckr = (comments / chapters) * 100;

    const expectedScore = Math.round(bkr + ckr);
    expect(score).toBe(expectedScore);
  });

  it('should gracefully handle a generic work with 0 hits and no averages', () => {
    const work = {
      ...WORK,
      stats: {
        ...WORK.stats,
        hits: 0,
      },
    };
    const score = calculateScore(work);

    const { stats: { bookmarks, comments, chapters, kudos } } = work;
    const bkr = (bookmarks / kudos) * 100;
    const ckr = ((comments / chapters) / kudos) * 100;

    const expectedScore = Math.round(bkr + ckr);
    expect(score).toBe(expectedScore);
  });

  it('should gracefully handle a generic work with 0 bookmarks and no averages', () => {
    const work = {
      ...WORK,
      stats: {
        ...WORK.stats,
        bookmarks: 0,
      },
    };
    const score = calculateScore(work);

    const { stats: { bookmarks, comments, chapters, kudos, hits } } = work;
    const khr = (kudos / hits) * 100;
    const bhr = (bookmarks / hits) * 1000;
    const chr = ((comments / chapters) / hits) * 1000;

    const expectedScore = Math.round(khr + bhr + chr);
    expect(score).toBe(expectedScore);
  });

  it('should properly calculate a score for a generic work with averages', () => {
    const averages = {
      bookmarks: 100,
      kudos: 1000,
    };

    const score = calculateScore(WORK, averages);
    const { stats: { bookmarks, kudos, comments, chapters, hits } } = WORK;

    const khr = (kudos / hits) * 100;
    const bhr = (bookmarks / hits) * 1000;
    const chr = ((comments / chapters) / hits) * 1000;

    const akr = (kudos / averages.kudos) * 10;
    const abr = (bookmarks / averages.bookmarks) * 10;

    const expectedScore = Math.round(khr + bhr + chr + akr + abr);
    expect(score).toBe(expectedScore);
  });

  it('should gracefully handle a generic work with averages missing kudos', () => {
    const averages = {
      bookmarks: 100,
    };

    const score = calculateScore(WORK, averages);
    const { stats: { bookmarks, kudos, comments, chapters, hits } } = WORK;

    const khr = (kudos / hits) * 100;
    const bhr = (bookmarks / hits) * 1000;
    const chr = ((comments / chapters) / hits) * 1000;

    const akr = 0;
    const abr = (bookmarks / averages.bookmarks) * 10;

    const expectedScore = Math.round(khr + bhr + chr + akr + abr);
    expect(score).toBe(expectedScore);
  });

  it('should gracefully handle a generic work with averages missing bookmarks', () => {
    const averages = {
      kudos: 1000,
    };

    const score = calculateScore(WORK, averages);
    const { stats: { bookmarks, kudos, comments, chapters, hits } } = WORK;

    const khr = (kudos / hits) * 100;
    const bhr = (bookmarks / hits) * 1000;
    const chr = ((comments / chapters) / hits) * 1000;

    const akr = (kudos / averages.kudos) * 10;
    const abr = 0;

    const expectedScore = Math.round(khr + bhr + chr + akr + abr);
    expect(score).toBe(expectedScore);
  });

  it('should gracefully handle hidden hits works', () => {
    const averages = {
      kudos: 1000,
      bookmarks: 100,
    };

    const work = {
      ...WORK,
      stats: {
        ...WORK.stats,
        hits: 0,
      },
    };

    const score = calculateScore(work, averages);
    const { stats: { bookmarks, kudos, comments, chapters } } = work;

    const bkr = (bookmarks / kudos) * 100;
    const ckr = ((comments / chapters) / kudos) * 100;

    const akr = (kudos / averages.kudos) * 10;
    const abr = (bookmarks / averages.bookmarks) * 10;

    const expectedScore = Math.round(bkr + ckr + akr + abr);
    expect(score).toBe(expectedScore);
  });
});

describe('getRandomInRange()', () => {
  afterEach(() => {
    jest.spyOn(global.Math, 'random').mockRestore();
  });

  it('should generate a random number between the values', () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0.5);

    const random = getRandomInRange(1, 3);
    expect(random).toBe(2);
  });

  it('should be inclusive of min value', () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0);

    const random = getRandomInRange(1, 3);
    expect(random).toBe(1);
  });

  it('should be inclusive of max value', () => {
    jest.spyOn(global.Math, 'random').mockReturnValue(0.99);

    const random = getRandomInRange(1, 3);
    expect(random).toBe(3);
  });
});

describe('getRelativeTime()', () => {
  it('should return a string', () => {
    const time = Date.now();
    const relativeTime = getRelativeTime(time);

    expect(typeof relativeTime).toBe('string');
  });

  // TODO: actually cover functionality here
});

describe('decodeAo3Url()', () => {
  it('should handle a basic URI', () => {
    const uri = 'fandom1';
    const decoded = decodeAo3Url(uri);

    expect(decoded).toBe('fandom1');
  });

  it('should handle a URI with generic encoding', () => {
    const uri = 'something%20&%20something%20else~%21';
    const decoded = decodeAo3Url(uri);

    expect(decoded).toBe('something & something else~!');
  });

  it('should handle a URI with AO3 encoding', () => {
    const uri = 'James%20T*d*%20Kirk*s*Spock';
    const decoded = decodeAo3Url(uri);

    expect(decoded).toBe('James T. Kirk/Spock');
  });
});

describe('getEncodedTag()', () => {
  it('should handle missing urls', () => {
    const tag = getEncodedTag();

    expect(tag).toBeNull();
  });

  it('should handle incorrect urls', () => {
    const notAUrl = 'something';
    const notATagUrl = 'https://archiveofourown.org/works/11593152/chapters/26056887';

    expect(getEncodedTag(notAUrl)).toBeNull();
    expect(getEncodedTag(notATagUrl)).toBeNull();
  });

  it('should get the encoded tag for valid urls', () => {
    const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';
    const tag = getEncodedTag(url);

    expect(tag).toBe('James%20T*d*%20Kirk*s*Spock');
  });
});

describe('getPage()', () => {
  it('should handle missing urls', () => {
    const page = getPage();

    expect(page).toBeNull();
  });

  it('should handle incorrect urls', () => {
    const notAUrl = 'something';
    const notAPageUrl = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';

    expect(getPage(notAUrl)).toBeNull();
    expect(getPage(notAPageUrl)).toBeNull();
  });

  it('should get the page number for valid urls', () => {
    const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works?page=456';
    const page = getPage(url);

    expect(page).toBe('456');
  });
});

describe('getTagUrl()', () => {
  it('should handle missing urls', () => {
    const tagUrl = getTagUrl();

    expect(tagUrl).toBeNull();
  });

  it('should return a tag url stripped of any additional info', () => {
    const simpleTag = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock';
    const complexTag = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works?page=456';

    expect(getTagUrl(simpleTag)).toBe(simpleTag);
    expect(getTagUrl(complexTag)).toBe(simpleTag);
  });
});

describe('getWorkId()', () => {
  it('should handle missing urls', () => {
    const id = getWorkId();

    expect(id).toBeNull();
  });

  it('should handle incorrect urls', () => {
    const notAUrl = 'something';
    const notAWorkUrl = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';

    expect(getWorkId(notAUrl)).toBeNull();
    expect(getWorkId(notAWorkUrl)).toBeNull();
  });

  it('should return an id for a given work url', () => {
    const url = 'https://archiveofourown.org/works/11593152/chapters/26056887';
    const id = getWorkId(url);

    expect(id).toBe('11593152');
  });
});

describe('getNextName()', () => {
  it('should start counts at name array length when no counts exist', () => {
    expect(getNextName('Test', [])).toBe('Test 1');
    expect(getNextName('Test', ['Something'])).toBe('Test 2');
    expect(getNextName('Test', ['a', 'b', 'c', 'd'])).toBe('Test 5');
  });

  it('should not consider numbers counts unless they match the expected pattern', () => {
    expect(getNextName('Test', ['2'])).toBe('Test 2');
    expect(getNextName('Test', ['Something 13'])).toBe('Test 2');
    expect(getNextName('Test', ['5 Test'])).toBe('Test 2');
  });

  it('should start counts after largest count present', () => {
    expect(getNextName('Test', ['Test 1'])).toBe('Test 2');
    expect(getNextName('Test', ['Test 11'])).toBe('Test 12');
    expect(getNextName('Test', ['Test 1', 'Test 2', 'Test 5'])).toBe('Test 6');
  });

  it('should trim space if not type is empty', () => {
    expect(getNextName('', [])).toBe('1');
    expect(getNextName(undefined, ['blegh'])).toBe('2');
  });

  it('should properly match counts for empty type', () => {
    expect(getNextName('', ['3'])).toBe('4');
    expect(getNextName(undefined, ['1', '5'])).toBe('6');
  });
});

describe('makeRegexFriendly()', () => {
  it('should handle a regex friendly string', () => {
    const string = 'something';
    const friendly = makeRegexFriendly(string);

    expect(friendly).toBe(string);
  });

  it('should handle a non-regex friendly string', () => {
    const string = 'i^d*k? some|stuff (here)';
    const expectedFriendly = 'i\\^d\\*k\\? some\\|stuff \\(here\\)';

    const friendly = makeRegexFriendly(string);
    expect(friendly).toBe(expectedFriendly);
  });
});

describe('truncate()', () => {
  it('should handle a string under the trunc limit', () => {
    const string = 'something';
    expect(truncate(string)).toBe(string);
  });

  it('should handle a string over the trunc limit', () => {
    const string = 'something';
    const truncated = truncate(string, 5, '?');

    expect(truncated).toBe('some?');
  });

  it('should consider trailing text length in string limit', () => {
    const string = 'something';
    const truncated = truncate(string, 5, '...');

    expect(truncated).toHaveLength(5);
  });

  it('should use default limit of 1024', () => {
    const truncated = truncate(TEXT_LONGER_THAN_1024);

    expect(truncated).toHaveLength(1024);
  });

  it('should use default trailing of "..."', () => {
    const string = 'something';
    const truncated = truncate(string, 5);

    expect(truncated).toBe('so...');
  });

  it('should throw if the limit is less than the trailing length', () => {
    expect(() => {
      truncate('', 2, '...');
    }).toThrow();
  });
});
