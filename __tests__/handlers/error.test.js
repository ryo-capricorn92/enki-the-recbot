const { EmbedBuilder } = require('discord.js');

const { TYPES, EnkiError } = require('../../src/helpers/error');

describe('EnkiError', () => {
  let spy;

  afterEach(() => {
    if (spy) {
      spy.mockClear();
    }
  });

  it('should mark an unknown error type as not an enki error', () => {
    const error = new EnkiError(null);
    expect(error.isEnkiError).toBeFalsy();
  });

  it('should handle an untracked tag error', () => {
    spy = jest.spyOn(EnkiError.prototype, 'mainTagUntracked');

    const error = new EnkiError(TYPES.MAIN_TAG_UNTRACKED, { tag: 'unknown' });

    expect(spy).toHaveBeenCalled();
    expect(error.type).toBe(TYPES.MAIN_TAG_UNTRACKED);
    expect(error.payload.isEmbed).toBe(true);
    expect(error.payload.embed).toBeInstanceOf(EmbedBuilder);
  });

  it('should handle a no works found error', () => {
    spy = jest.spyOn(EnkiError.prototype, 'noWorksFound');

    const error = new EnkiError(TYPES.NO_WORKS_FOUND);

    expect(spy).toHaveBeenCalled();
    expect(error.type).toBe(TYPES.NO_WORKS_FOUND);
    expect(error.payload.isEmbed).toBe(true);
    expect(error.payload.embed).toBeInstanceOf(EmbedBuilder);
  });

  it('should handle a work not found error', () => {
    spy = jest.spyOn(EnkiError.prototype, 'workNotFound');

    const error = new EnkiError(TYPES.WORK_NOT_FOUND);

    expect(spy).toHaveBeenCalled();
    expect(error.type).toBe(TYPES.WORK_NOT_FOUND);
    expect(error.payload.isEmbed).toBe(true);
    expect(error.payload.embed).toBeInstanceOf(EmbedBuilder);
  });

  it('should handle a locked work error', () => {
    spy = jest.spyOn(EnkiError.prototype, 'workLocked');

    const error = new EnkiError(TYPES.LOCKED_WORK);

    expect(spy).toHaveBeenCalled();
    expect(error.type).toBe(TYPES.LOCKED_WORK);
    expect(error.payload.isEmbed).toBe(true);
    expect(error.payload.embed).toBeInstanceOf(EmbedBuilder);
  });

  it('should handle a throttling error', () => {
    spy = jest.spyOn(EnkiError.prototype, 'throttled');

    const error = new EnkiError(TYPES.THROTTLED, { timeout: Date.now() });

    expect(spy).toHaveBeenCalled();
    expect(error.type).toBe(TYPES.THROTTLED);
    expect(error.payload.isEmbed).toBe(true);
    expect(error.payload.embed).toBeInstanceOf(EmbedBuilder);
  });
});
