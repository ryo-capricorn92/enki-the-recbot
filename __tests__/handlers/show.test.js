const mockInteract = jest.requireActual('../../__mocks__/discord.js/CommandInteraction');
const mockCollection = jest.requireActual('../../__mocks__/mongodb/collection');

const mockScrape = require('../../__mocks__/src/scrape');

const { WORK, FANDOM_TAG } = require('../../__mocks__/__data__');

jest.mock('discord.js');
jest.mock('mongodb');
jest.mock('../../src/scrape', () => mockScrape);

const { Mongo } = require('../../src/mongo');
const { ShowHandler } = require('../../src/handlers/show');
const { DiscordDisplay } = require('../../src/helpers/display');
const { EnkiError } = require('../../src/helpers/error');

describe('ShowHandler', () => {
  let mongo;
  beforeAll(async () => {
    mongo = new Mongo();
    mongo.init();
    ShowHandler.prototype.timeoutAfter = 1000;
  });

  describe('constructor()', () => {
    it('should construct a basic handler', () => {
      const options = {};
      const throttling = {
        throttled: false,
      };

      const interaction = new mockInteract.CommandInteraction(options);

      const handler = new ShowHandler(interaction, { mongo, throttling });

      expect(handler.interaction).toBe(interaction);
      expect(handler.mongo).toBe(mongo);
    });

    it('should throw an enki error if throttled', () => {
      const options = {};
      const throttling = {
        throttled: true,
        timeout: Date.now(),
      };

      const interaction = new mockInteract.CommandInteraction(options);

      expect(() => new ShowHandler(interaction, { mongo, throttling })).toThrow(EnkiError);
    });
  });

  describe('validate()', () => {
    it('should call deferReply regardless of whether the url is valid', () => {
      const throttling = {
        throttled: false,
      };

      const interactInvalid = new mockInteract.CommandInteraction({ url: 'something' });
      const handlerInvalid = new ShowHandler(interactInvalid, { mongo, throttling });

      mockInteract.deferReply.mockReset();
      handlerInvalid.validate();
      expect(mockInteract.deferReply).toHaveBeenCalledTimes(1);

      const interactValid = new mockInteract.CommandInteraction({ url: 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works?page=456' });
      const handlerValid = new ShowHandler(interactValid, { mongo, throttling });

      mockInteract.deferReply.mockReset();
      handlerValid.validate();
      expect(mockInteract.deferReply).toHaveBeenCalledTimes(1);
    });

    it('should return true for valid urls', () => {
      const throttling = {
        throttled: false,
      };

      const validURLs = [
        'https://archiveofourown.org/works/11593152/chapters/26056887',
        'https://archiveofourown.org/works/22114921/',
        'https://archiveofourown.org/works/22114921',
      ];

      validURLs.forEach((url) => {
        const interaction = new mockInteract.CommandInteraction({ url });
        const handler = new ShowHandler(interaction, { mongo, throttling });
        expect(handler.validate()).resolves.toBe(true);
      });
    });

    it('should return false for invalid urls', () => {
      const throttling = {
        throttled: false,
      };

      const invalidURLs = [
        'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works?page=456',
        'https://archiveofourown.org/tags/Sherlock%20Holmes*s*John%20Watson',
        'https://archiveofourown.org/works?commit=Sort+and+Filter&work_search%5Bsort_column%5D=kudos_count&include_work_search%5Brating_ids%5D%5B%5D=13&work_search%5Bother_tag_names%5D=&work_search%5Bexcluded_tag_names%5D=&work_search%5Bcrossover%5D=&work_search%5Bcomplete%5D=&work_search%5Bwords_from%5D=&work_search%5Bwords_to%5D=&work_search%5Bdate_from%5D=&work_search%5Bdate_to%5D=&work_search%5Bquery%5D=&work_search%5Blanguage_id%5D=&tag_id=Geralt+z+Rivii+%7C+Geralt+of+Rivia*s*Jaskier+%7C+Dandelion',
        'bloop',
      ];

      invalidURLs.forEach((url) => {
        const interaction = new mockInteract.CommandInteraction({ url });
        const handler = new ShowHandler(interaction, { mongo, throttling });
        expect(handler.validate()).resolves.toBe(false);
      });
    });
  });

  describe('addTrackedTags()', () => {
    it('should overwrite the work\'s tracked tags with those returned by findTrackedTags', async () => {
      const throttling = {
        throttled: false,
      };

      const interaction = new mockInteract.CommandInteraction({});
      const handler = new ShowHandler(interaction, { mongo, throttling });

      mockCollection.find.mockReset();
      mockCollection.find.mockImplementationOnce(() => ([FANDOM_TAG]));

      const workWithTags = await handler.addTrackedTags(WORK);

      expect(workWithTags.trackedTags).toHaveLength(1);
      expect(workWithTags.trackedTags).not.toStrictEqual(WORK.trackedTags);
    });
  });

  describe('handle()', () => {
    const displaySpies = {
      addRec: jest.spyOn(DiscordDisplay.prototype, 'addRec'),
    };

    beforeEach(async () => {
      mockCollection.find.mockReset();
      mockCollection.findOne.mockReset();
      mockCollection.insertOne.mockReset();
      mockCollection.updateOne.mockReset();
      mockScrape.scrapeWork.mockReset();
      displaySpies.addRec.mockClear();

      mockCollection.find.mockImplementation(() => ([]));
      mockCollection.findOne.mockImplementation(async () => ({}));
      mockCollection.insertOne.mockImplementation(async () => ({}));
      mockCollection.updateOne.mockImplementation(async () => ({}));
    });

    it('should validate the payload and handle invalids', async () => {
      const throttling = {
        throttled: false,
      };

      const interaction = new mockInteract.CommandInteraction({ url: 'not a url' });
      const handler = new ShowHandler(interaction, { mongo, throttling });

      const spies = {
        validate: jest.spyOn(handler, 'validate'),
        handleInvalid: jest.spyOn(handler, 'handleInvalid'),
        ingestWork: jest.spyOn(handler, 'ingestWork'),
      };

      await handler.handle();

      expect(spies.validate).toHaveBeenCalled();
      expect(spies.handleInvalid).toHaveBeenCalled();
      expect(spies.ingestWork).not.toHaveBeenCalled();
    });

    it('should display the work directly from the database when available', async () => {
      const throttling = {
        throttled: false,
      };

      const fromDB = {
        ...WORK,
        title: 'from-db',
      };

      const fromScrape = {
        ...WORK,
        title: 'from-scrape',
      };

      const interaction = new mockInteract.CommandInteraction({ url: WORK.link });
      const handler = new ShowHandler(interaction, { mongo, throttling });

      mockCollection.findOne.mockImplementationOnce(async () => fromDB);
      mockScrape.scrapeWork.mockImplementationOnce(async () => fromScrape);

      await handler.handle();

      expect(mockCollection.findOne).toHaveBeenCalled();
      expect(mockScrape.scrapeWork).not.toHaveBeenCalled();
      expect(displaySpies.addRec).toHaveBeenCalledWith(fromDB);
    });

    it('should scrape the work when no work is available in the DB', async () => {
      const throttling = {
        throttled: false,
      };

      const fromScrape = {
        ...WORK,
        title: 'from-scrape',
      };

      const interaction = new mockInteract.CommandInteraction({ url: WORK.link });
      const handler = new ShowHandler(interaction, { mongo, throttling });

      mockCollection.findOne.mockImplementationOnce(async () => null);
      mockScrape.scrapeWork.mockImplementationOnce(async () => fromScrape);

      await handler.handle();

      expect(mockCollection.findOne).toHaveBeenCalled();
      expect(mockScrape.scrapeWork).toHaveBeenCalled();
      expect(displaySpies.addRec).toHaveBeenCalledWith(fromScrape);
    });

    it('should should scrape the work regardless of database entries when user uses getfresh', async () => {
      const throttling = {
        throttled: false,
      };

      const fromDB = {
        ...WORK,
        title: 'from-db',
      };

      const fromScrape = {
        ...WORK,
        title: 'from-scrape',
      };

      const interaction = new mockInteract.CommandInteraction({ url: WORK.link, getfresh: true });
      const handler = new ShowHandler(interaction, { mongo, throttling });

      mockCollection.findOne.mockImplementationOnce(async () => fromDB);
      mockScrape.scrapeWork.mockImplementationOnce(async () => fromScrape);

      await handler.handle();

      expect(mockCollection.findOne).not.toHaveBeenCalled();
      expect(mockScrape.scrapeWork).toHaveBeenCalled();
      expect(displaySpies.addRec).toHaveBeenCalledWith(fromScrape);
    });
  });
});
