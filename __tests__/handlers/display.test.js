const {
  ActionRowBuilder,
  ButtonBuilder,
  EmbedBuilder,
  StringSelectMenuBuilder,
} = require('discord.js');

const { DiscordDisplay } = require('../../src/helpers/display');

const { WORK, TEXT_LONGER_THAN_1024, SESSION, USER, FakeObjectId } = require('../../__mocks__/__data__');

describe('DiscordDisplay', () => {
  describe('getPayload()', () => {
    it('should return the stored payload', () => {
      const display = new DiscordDisplay()
        .addRec(WORK)
        .addWorkOptions();

      expect(display.getPayload()).toBe(display.payload);
    });

    it('should throw an error if no message is populated', () => {
      const display = new DiscordDisplay();
      expect(() => display.getPayload()).toThrow();

      display.payload.content = '';
      expect(() => display.getPayload()).toThrow();
    });
  });

  describe('addRec()', () => {
    it('should format a generic work into a rec embed', () => {
      const payload = new DiscordDisplay()
        .addRec(WORK)
        .getPayload();
      const [embed] = payload.embeds;

      expect(embed).toBeInstanceOf(EmbedBuilder);
      expect(embed.data.title).toBe(WORK.title);
      expect(embed.data.url).toBe(WORK.link);
      expect(embed.data.author.name).toBe('writer');
      expect(embed.data.color).toBe(15698226);
      expect(embed.data.footer.text).toBe('Author last updated on 05 Nov 2024 - | - Enki last updated on 27 Dec 2024');

      const fields = [
        { name: 'Rating', value: WORK.rating, inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Chapters', value: WORK.chapters, inline: true },
        { name: 'Warnings', value: 'Death', inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Words', value: '5,000', inline: true },
        { name: 'Fandoms', value: '__fandom1__' },
        { name: 'Relationship(s)', value: '__guy/dude__' },
        { name: 'Tags', value: '__freeform1__, __synonym2__' },
        { name: 'Summary', value: 'stuff happens' },
      ];

      expect(embed.data.fields).toStrictEqual(fields);
    });

    it('should format an edgecase work into a rec embed', () => {
      const work = {
        ...WORK,
        authors: ['one', 'two'],
        warnings: [],
        relationships: [],
        summary: TEXT_LONGER_THAN_1024,
      };

      const payload = new DiscordDisplay()
        .addRec(work)
        .getPayload();
      const [embed] = payload.embeds;

      expect(embed).toBeInstanceOf(EmbedBuilder);
      expect(embed.data.title).toBe(work.title);
      expect(embed.data.url).toBe(work.link);
      expect(embed.data.author.name).toBe('one, two');
      expect(embed.data.color).toBe(15698226);

      const fields = [
        { name: 'Rating', value: WORK.rating, inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Chapters', value: WORK.chapters, inline: true },
        { name: 'Warnings', value: 'None', inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Words', value: '5,000', inline: true },
        { name: 'Fandoms', value: '__fandom1__' },
        { name: 'Relationship(s)', value: 'None' },
        { name: 'Tags', value: '__freeform1__, __synonym2__' },
        { name: 'Summary', value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque consectetur sem id nisl congue varius. Etiam interdum lacinia mi sit amet aliquet. Sed eu rutrum eros, ac ultricies nibh. Proin sollicitudin volutpat purus ut fermentum. Nulla euismod ex et nulla tempor, egestas malesuada nisi luctus. Pellentesque dapibus ipsum eget risus dictum cursus. Ut blandit sollicitudin semper. Cras ut suscipit leo. Proin felis lorem, efficitur sit amet augue a, rhoncus pretium libero. In tempor erat a volutpat suscipit. Phasellus sollicitudin elit in justo mollis lacinia. Pellentesque a sapien eu nisl rutrum luctus. Integer rhoncus condimentum sem, sed pharetra neque rhoncus a. Vestibulum iaculis urna vitae dignissim eleifend. Sed ac metus lacus. Sed sed interdum risus. Integer sit amet ipsum quis mi pellentesque ullamcorper. Aliquam auctor sem neque, scelerisque facilisis lorem vestibulum eu. Phasellus ornare suscipit mauris non ornare. Cras pharetra imperdiet turpis, in feugiat metus eleifend eget. Nulla sodales neq...' },
      ];

      expect(embed.data.fields).toStrictEqual(fields);
    });
  });

  describe('addWorkOptions()', () => {
    it('should add a component row with 3 buttons', () => {
      const payload = new DiscordDisplay()
        .addWorkOptions()
        .getPayload();

      expect(payload.components).toHaveLength(1);

      const { components } = payload.components[0];
      expect(components).toHaveLength(3);

      components.forEach((c) => {
        expect(c).toBeInstanceOf(ButtonBuilder);
      });
    });

    it('should add the expected buttons', () => {
      const expectedIds = ['markAsRead', 'markAsIgnored', 'addToList'];
      const payload = new DiscordDisplay()
        .addWorkOptions()
        .getPayload();

      const ids = payload.components[0].components.map((c) => c.data.custom_id);
      expect(ids).toStrictEqual(expectedIds);
    });
  });

  describe('addSessionDetails()', () => {
    it('should add the session details embed to the payload', () => {
      const payload = new DiscordDisplay()
        .addSessionDetails(SESSION, USER._id)
        .getPayload();

      const fields = [
        { name: 'Session Name', value: SESSION.name, inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Session Owner', value: '<@321>', inline: true },
        { name: 'Read Works', value: `${SESSION.read.length}`, inline: true },
        { name: '\u200b', value: '\u200b', inline: true },
        { name: 'Ignored Works', value: `${SESSION.ignored.length}`, inline: true },
      ];

      expect(payload.embeds).toHaveLength(1);
      const [embed] = payload.embeds;
      expect(embed).toBeInstanceOf(EmbedBuilder);
      expect(embed.data.fields).toStrictEqual(fields);
    });
  });

  describe('addManageSessionButtons()', () => {
    it('should add a component row with 3 buttons', () => {
      const payload = new DiscordDisplay()
        .addManageSessionButtons()
        .getPayload();

      expect(payload.components).toHaveLength(1);

      const { components } = payload.components[0];
      expect(components).toHaveLength(3);

      components.forEach((c) => {
        expect(c).toBeInstanceOf(ButtonBuilder);
      });
    });

    it('should add the expected buttons', () => {
      const expectedIds = ['addNewSession', 'changeSession', 'deleteSession'];
      const payload = new DiscordDisplay()
        .addManageSessionButtons()
        .getPayload();

      const ids = payload.components[0].components.map((c) => c.data.custom_id);
      expect(ids).toStrictEqual(expectedIds);
    });
  });

  describe('addPruneSessionButtons()', () => {
    it('should add a component row with 2 buttons', () => {
      const payload = new DiscordDisplay()
        .addPruneSessionButtons()
        .getPayload();

      expect(payload.components).toHaveLength(1);

      const { components } = payload.components[0];
      expect(components).toHaveLength(2);

      components.forEach((c) => {
        expect(c).toBeInstanceOf(ButtonBuilder);
      });
    });

    it('should add the expected buttons', () => {
      const expectedIds = ['resetRead', 'resetIgnored'];
      const payload = new DiscordDisplay()
        .addPruneSessionButtons()
        .getPayload();

      const ids = payload.components[0].components.map((c) => c.data.custom_id);
      expect(ids).toStrictEqual(expectedIds);
    });
  });

  describe('addChangeSession()', () => {
    it('should add a select component row and a button row', () => {
      const payload = new DiscordDisplay()
        .addChangeSession([], SESSION._id)
        .getPayload();

      expect(payload.components).toHaveLength(2);

      const selectRow = payload.components[0];
      const buttonRow = payload.components[1];

      expect(selectRow.components).toHaveLength(1);
      expect(buttonRow.components).toHaveLength(1);

      expect(selectRow.components[0]).toBeInstanceOf(StringSelectMenuBuilder);
      expect(buttonRow.components[0]).toBeInstanceOf(ButtonBuilder);
    });

    it('should format the options properly', () => {
      const otherSession = {
        ...SESSION,
        _id: new FakeObjectId('other_session_id'),
        name: 'Session 2',
        read: [WORK._id],
      };

      const payload = new DiscordDisplay()
        .addChangeSession([SESSION, otherSession], SESSION._id)
        .getPayload();

      const { options } = payload.components[0].components[0];

      const expected = [{
        label: 'default',
        description: '0 read | 0 ignored',
        value: 'ghi',
      }, {
        label: 'Session 2',
        description: '1 read | 0 ignored',
        value: 'other_session_id',
      }];

      const received = options
        .map(({ data }) => ({
          label: data.label,
          description: data.description,
          value: data.value,
        }));

      expect(received).toStrictEqual(expected);
    });

    it('should pick the correct default', () => {
      const otherSession = {
        ...SESSION,
        _id: new FakeObjectId('other_session_id'),
      };

      const payload = new DiscordDisplay()
        .addChangeSession([SESSION, otherSession], otherSession._id)
        .getPayload();

      const { options } = payload.components[0].components[0];

      const expected = [{
        value: 'ghi',
        default: false,
      }, {
        value: 'other_session_id',
        default: true,
      }];

      const received = options
        .map(({ data }) => ({
          value: data.value,
          default: data.default,
        }));

      expect(received).toStrictEqual(expected);
    });
  });

  describe('addDeleteSession()', () => {
    it('should add an embed and a row with 2 buttons', () => {
      const payload = new DiscordDisplay()
        .addDeleteSession()
        .getPayload();

      expect(payload.embeds).toHaveLength(1);
      expect(payload.components).toHaveLength(1);

      const { components } = payload.components[0];
      expect(components).toHaveLength(2);

      components.forEach((c) => {
        expect(c).toBeInstanceOf(ButtonBuilder);
      });
    });

    it('should add the expected buttons', () => {
      const expectedIds = ['confirmSessionDelete', 'cancelSessionDelete'];
      const payload = new DiscordDisplay()
        .addDeleteSession()
        .getPayload();

      const ids = payload.components[0].components.map((c) => c.data.custom_id);
      expect(ids).toStrictEqual(expectedIds);
    });
  });

  describe('addResetRead()', () => {
    it('should add an embed and a row with 2 buttons', () => {
      const payload = new DiscordDisplay()
        .addResetRead()
        .getPayload();

      expect(payload.embeds).toHaveLength(1);
      expect(payload.components).toHaveLength(1);

      const { components } = payload.components[0];
      expect(components).toHaveLength(2);

      components.forEach((c) => {
        expect(c).toBeInstanceOf(ButtonBuilder);
      });
    });

    it('should add the expected buttons', () => {
      const expectedIds = ['confirmReadReset', 'cancelReadReset'];
      const payload = new DiscordDisplay()
        .addResetRead()
        .getPayload();

      const ids = payload.components[0].components.map((c) => c.data.custom_id);
      expect(ids).toStrictEqual(expectedIds);
    });
  });

  describe('addResetIgnored()', () => {
    it('should add an embed and a row with 2 buttons', () => {
      const payload = new DiscordDisplay()
        .addResetIgnored()
        .getPayload();

      expect(payload.embeds).toHaveLength(1);
      expect(payload.components).toHaveLength(1);

      const { components } = payload.components[0];
      expect(components).toHaveLength(2);

      components.forEach((c) => {
        expect(c).toBeInstanceOf(ButtonBuilder);
      });
    });

    it('should add the expected buttons', () => {
      const expectedIds = ['confirmIgnoredReset', 'cancelIgnoredReset'];
      const payload = new DiscordDisplay()
        .addResetIgnored()
        .getPayload();

      const ids = payload.components[0].components.map((c) => c.data.custom_id);
      expect(ids).toStrictEqual(expectedIds);
    });
  });

  describe('addWelcomeMessage()', () => {
    it('should add a welcome embed to the payload', () => {
      const payload = new DiscordDisplay()
        .addWelcomeMessage()
        .getPayload();

      expect(payload.embeds).toHaveLength(1);
      const [embed] = payload.embeds;
      expect(embed).toBeInstanceOf(EmbedBuilder);
      expect(embed.data.title).toBe('Welcome to Enki!');
    });
  });

  describe('makeStatic()', () => {
    it('should return a generic message if no payload exists', () => {
      const payload = new DiscordDisplay()
        .makeStatic()
        .getPayload();

      const expected = {
        content: 'This message has expired',
        components: [],
      };

      expect(payload).toStrictEqual(expected);
    });

    it('should overwrite basic content', () => {
      const basicMessage = {
        content: 'Some test message',
      };

      const payload = new DiscordDisplay(basicMessage)
        .makeStatic()
        .getPayload();

      const expected = {
        content: 'This message has expired',
        components: [],
      };

      expect(payload).toStrictEqual(expected);
    });

    it('should remove components from a basic content message', () => {
      const basicMessage = {
        content: 'Some test message',
        components: [new ActionRowBuilder().addComponents(new ButtonBuilder())],
      };

      const payload = new DiscordDisplay(basicMessage)
        .makeStatic()
        .getPayload();

      const expected = {
        content: 'This message has expired',
        components: [],
      };

      expect(payload).toStrictEqual(expected);
    });

    it('should remove components from an embed message', () => {
      const basicMessage = {
        embeds: [new EmbedBuilder()],
        components: [new ActionRowBuilder().addComponents(new ButtonBuilder())],
      };

      const payload = new DiscordDisplay(basicMessage)
        .makeStatic()
        .getPayload();

      expect(payload.components).toHaveLength(0);
      expect(payload.embeds[0].data.footer.text).toBe('This message has expired');
    });
  });
});
