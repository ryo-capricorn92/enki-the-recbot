const mockDiscordjs = jest.requireActual('../../__mocks__/discord.js');
const mockInteract = jest.requireActual('../../__mocks__/discord.js/CommandInteraction');

const mockScrape = require('../../__mocks__/src/scrape');
const mockMongo = require('../../__mocks__/src/mongo');
const mockMongoWork = require('../../__mocks__/src/mongo/work');
const mockMongoTag = require('../../__mocks__/src/mongo/tag');

const { FANDOM_TAG, FREEFORM_TAG_1, FREEFORM_TAG_2, WORK } = require('../../__mocks__/__data__');

jest.mock('discord.js');
jest.mock('../../src/scrape', () => mockScrape);

const { ScrapeHandler } = require('../../src/handlers/scrape');
const { EnkiError } = require('../../src/helpers/error');

describe('ScrapeHandler', () => {
  ScrapeHandler.prototype.timeoutAfter = 1000;

  describe('constructor()', () => {
    it('should construct a basic handler', () => {
      const options = {};
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interaction = new mockDiscordjs.CommandInteraction(options);

      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      expect(handler.interaction).toBe(interaction);
      expect(handler.mongo).toBe(mongo);
    });

    it('should throw an enki error if throttled', () => {
      const options = {};
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: true,
        timeout: Date.now(),
      };

      const interaction = new mockDiscordjs.CommandInteraction(options);

      expect(() => new ScrapeHandler(interaction, { mongo, throttling })).toThrow(EnkiError);
    });
  });

  describe('validate()', () => {
    it('should call deferReply regardless of whether the url is valid', () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interactInvalid = new mockDiscordjs.CommandInteraction({ url: 'something' });
      const handlerInvalid = new ScrapeHandler(interactInvalid, { mongo, throttling });

      mockInteract.deferReply.mockReset();
      handlerInvalid.validate();
      expect(mockInteract.deferReply).toHaveBeenCalledTimes(1);

      const interactValid = new mockDiscordjs.CommandInteraction({ url: 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works?page=456' });
      const handlerValid = new ScrapeHandler(interactValid, { mongo, throttling });

      mockInteract.deferReply.mockReset();
      handlerValid.validate();
      expect(mockInteract.deferReply).toHaveBeenCalledTimes(1);
    });

    it('should return true for valid urls', () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const validURLs = [
        'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works?page=456',
        'https://archiveofourown.org/tags/Sherlock%20Holmes*s*John%20Watson/works',
        'https://archiveofourown.org/tags/Will%20Graham*s*Hannibal%20Lecter/',
        'https://archiveofourown.org/tags/Geralt%20z%20Rivii%20%7C%20Geralt%20of%20Rivia*s*Jaskier%20%7C%20Dandelion',
      ];

      validURLs.forEach((url) => {
        const interaction = new mockDiscordjs.CommandInteraction({ url });
        const handler = new ScrapeHandler(interaction, { mongo, throttling });
        expect(handler.validate()).resolves.toBe(true);
      });
    });

    it('should return false for invalid urls', () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const invalidURLs = [
        'https://archiveofourown.org/tags/Sherlock%20Holmes*s*John%20Watson/blurgh',
        'https://archiveofourown.org/works?commit=Sort+and+Filter&work_search%5Bsort_column%5D=kudos_count&include_work_search%5Brating_ids%5D%5B%5D=13&work_search%5Bother_tag_names%5D=&work_search%5Bexcluded_tag_names%5D=&work_search%5Bcrossover%5D=&work_search%5Bcomplete%5D=&work_search%5Bwords_from%5D=&work_search%5Bwords_to%5D=&work_search%5Bdate_from%5D=&work_search%5Bdate_to%5D=&work_search%5Bquery%5D=&work_search%5Blanguage_id%5D=&tag_id=Geralt+z+Rivii+%7C+Geralt+of+Rivia*s*Jaskier+%7C+Dandelion',
        'https://archiveofourown.org/works/22114921',
        'bloop',
      ];

      invalidURLs.forEach((url) => {
        const interaction = new mockDiscordjs.CommandInteraction({ url });
        const handler = new ScrapeHandler(interaction, { mongo, throttling });
        expect(handler.validate()).resolves.toBe(false);
      });
    });
  });

  describe('addTrackedTags()', () => {
    it('should call findTrackedTags on the given work', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interaction = new mockDiscordjs.CommandInteraction({});
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockMongoTag.findTrackedTags.mockReset();
      mockMongoTag.findTrackedTags.mockImplementation(async () => ([FANDOM_TAG._id]));
      await handler.addTrackedTags(WORK);
      expect(mockMongoTag.findTrackedTags).toHaveBeenCalledTimes(1);
      expect(mockMongoTag.findTrackedTags).toHaveBeenCalledWith([WORK]);
    });

    it('should overwrite the work\'s tracked tags with those returned by findTrackedTags', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interaction = new mockDiscordjs.CommandInteraction({});
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockMongoTag.findTrackedTags.mockReset();
      mockMongoTag.findTrackedTags.mockImplementation(async () => ([FANDOM_TAG._id]));
      const newWork = await handler.addTrackedTags(WORK);

      expect(newWork._id).toBe(WORK._id);
      expect(newWork.trackedTags).toStrictEqual([FANDOM_TAG._id]);
    });
  });

  describe('handleTagInit()', () => {
    it('should scrape the top three pages of works', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interaction = new mockDiscordjs.CommandInteraction({ url: 'https://archiveofourown.org/tags/testing' });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockScrape.scrapeTaggedWorks.mockReset();
      mockScrape.scrapeTaggedWorks
        .mockImplementation(async () => ({ pageCount: 5, works: [WORK] }));
      mockScrape.scrapeMasterTag.mockImplementation(async () => FANDOM_TAG);

      await handler.handleTagInit();

      expect(mockScrape.scrapeTaggedWorks).toHaveBeenCalledTimes(3);
      expect(mockScrape.scrapeTaggedWorks).toHaveBeenCalledWith('https://archiveofourown.org/works?page=1&work_search%5Bsort_column%5D=kudos_count&tag_id=testing');
      expect(mockScrape.scrapeTaggedWorks).toHaveBeenCalledWith('https://archiveofourown.org/works?page=2&work_search%5Bsort_column%5D=kudos_count&tag_id=testing');
      expect(mockScrape.scrapeTaggedWorks).toHaveBeenCalledWith('https://archiveofourown.org/works?page=3&work_search%5Bsort_column%5D=kudos_count&tag_id=testing');
    });

    it('should ingest all the works at once', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interaction = new mockDiscordjs.CommandInteraction({ url: 'https://archiveofourown.org/tags/testing' });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockScrape.scrapeTaggedWorks
        .mockImplementation(async () => ({ pageCount: 5, works: [WORK] }));
      mockScrape.scrapeMasterTag.mockImplementation(async () => FANDOM_TAG);

      await handler.handleTagInit();

      const expected = {
        works: [WORK, WORK, WORK],
        pageCount: 5,
      };

      const lastCall = mockMongoWork.ingestWorks.mock.lastCall[0];
      expect(lastCall[0]).toStrictEqual(expected);
    });

    it('should edit the existing reply', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const interaction = new mockDiscordjs.CommandInteraction({ url: 'https://archiveofourown.org/tags/testing' });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockScrape.scrapeTaggedWorks
        .mockImplementation(async () => ({ pageCount: 5, works: [WORK] }));
      mockScrape.scrapeMasterTag.mockImplementation(async () => FANDOM_TAG);

      await handler.handleTagInit();

      const expected = `Initated the \`${FANDOM_TAG.master}\` tag - tag and top works have been scraped.`;

      const lastCall = mockInteract.editReply.mock.lastCall[0];
      expect(lastCall).toStrictEqual(expected);
    });
  });

  describe('handleSingleScrape()', () => {
    it('should scrape and ingest works from the given url', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';
      const scrapedRes = { pageCount: 5, works: [WORK] };

      const interaction = new mockDiscordjs.CommandInteraction({ url });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockScrape.scrapeTaggedWorks
        .mockImplementation(async () => (scrapedRes));

      await handler.handleSingleScrape();

      // it should call scrapeTaggedWorks with the provided url
      expect(mockScrape.scrapeTaggedWorks).toHaveBeenLastCalledWith(url);

      // it should ingest works from the tag scrape
      expect(mockMongoWork.ingestWorks).toHaveBeenLastCalledWith([scrapedRes, 'James T. Kirk/Spock']);

      // it should edit the given reply
      const expected = 'Scraped one page of works from the `James T. Kirk/Spock` tag.';
      expect(mockInteract.editReply).toHaveBeenLastCalledWith(expected);
    });
  });

  describe('handleTagScrape()', () => {
    it('should scrape the given tag', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';

      const interaction = new mockDiscordjs.CommandInteraction({ url });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const spies = {
        scrapeTag: jest.spyOn(handler, 'scrapeTag'),
      };

      await handler.handleTagScrape();

      // it should trigger a tag scrape
      expect(spies.scrapeTag).toHaveBeenCalled();

      // it should edit the given reply
      const expected = 'Scraped the `James T. Kirk/Spock` tag.';
      expect(mockInteract.editReply).toHaveBeenLastCalledWith(expected);
    });
  });

  describe('handleUncommonTag()', () => {
    it('should create a button event and listen for actions', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';
      const uncommon = { ...FREEFORM_TAG_1, isCommon: false };

      const interaction = new mockDiscordjs.CommandInteraction({ url }, { id: '1' });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const message = await handler.handleUncommonTag(uncommon);

      mockInteract.editReply.mockReset();
      mockMongoTag.upsertTag.mockReset();

      const spies = {
        // eslint-disable-next-line no-underscore-dangle
        listener: jest.spyOn(message.collector._listenersForJest, 'collect'),
      };

      // it should create a listener on the message collector
      expect(message.collector.listenerCount('collect')).toBe(1);

      // it should return null if users don't match from event and interaction
      const res = await message.collector.emit('collect', { user: { id: '2' }, customId: '' });
      expect(spies.listener).toHaveBeenCalledTimes(1);
      expect(res).toBeNull();

      // it should not scrape the tag when the customId is not acceptTag
      await message.collector.emit('collect', { user: { id: '1' }, customId: '' });
      expect(spies.listener).toHaveBeenCalledTimes(2);
      expect(mockMongoTag.upsertTag).not.toHaveBeenCalled();
      expect(mockInteract.editReply.mock.lastCall[0].content)
        .toBe(`Uncommon tag \`${uncommon.master}\` has not been scraped.`);

      // it should scrape the tag when the customId is acceptTag
      await message.collector.emit('collect', { user: { id: '1' }, customId: 'acceptTag' });
      expect(spies.listener).toHaveBeenCalledTimes(3);
      expect(mockMongoTag.upsertTag).toHaveBeenLastCalledWith([uncommon]);
      expect(mockInteract.editReply.mock.lastCall[0].content)
        .toBe(`Uncommon tag \`${uncommon.master}\` has been scraped. No works have been scraped for it.`);
    });
  });

  describe('handle()', () => {
    it('should validate the payload and handle invalids', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const options = {
        url: 'not a url',
        type: 'init',
      };

      const interaction = new mockDiscordjs.CommandInteraction(options);
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const spies = {
        validate: jest.spyOn(handler, 'validate'),
        handleInvalid: jest.spyOn(handler, 'handleInvalid'),
        handleTagInit: jest.spyOn(handler, 'handleTagInit'),
      };

      await handler.handle();

      expect(spies.validate).toHaveBeenCalled();
      expect(spies.handleInvalid).toHaveBeenCalled();
      expect(spies.handleTagInit).not.toHaveBeenCalled();
    });

    it('should use handleTagInit for init type', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const options = {
        url: 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works',
        type: 'init',
      };

      const interaction = new mockDiscordjs.CommandInteraction(options);
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const spies = {
        handleTagInit: jest.spyOn(handler, 'handleTagInit'),
      };

      await handler.handle();

      expect(spies.handleTagInit).toHaveBeenCalled();
    });

    it('should use handleSingleScrape for single type', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const options = {
        url: 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works',
        type: 'single',
      };

      const interaction = new mockDiscordjs.CommandInteraction(options);
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const spies = {
        handleSingleScrape: jest.spyOn(handler, 'handleSingleScrape'),
      };

      await handler.handle();

      expect(spies.handleSingleScrape).toHaveBeenCalled();
    });

    it('should use handleTagScrape for tag type', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const options = {
        url: 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works',
        type: 'tag',
      };

      const interaction = new mockDiscordjs.CommandInteraction(options);
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const spies = {
        handleTagScrape: jest.spyOn(handler, 'handleTagScrape'),
      };

      await handler.handle();

      expect(spies.handleTagScrape).toHaveBeenCalled();
    });
  });

  describe('scrapetag()', () => {
    it('should scrape a given tag', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';

      const interaction = new mockDiscordjs.CommandInteraction({ url });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      mockScrape.scrapeMasterTag.mockReset();
      mockScrape.scrapeMasterTag.mockImplementation(async () => FANDOM_TAG);

      const tag = await handler.scrapeTag();

      // it should scrape the tag from the url
      expect(mockScrape.scrapeMasterTag).toHaveBeenCalledWith(url);

      // it should upsert the tag
      expect(mockMongoTag.upsertTag).toHaveBeenCalledWith([FANDOM_TAG]);

      // it should update all tracked tags on works
      expect(mockMongoWork.updateTrackedTags).toHaveBeenCalledWith([FANDOM_TAG]);

      // it should return the tag
      expect(tag).toBe(FANDOM_TAG);
    });

    it('should handle uncommon tags', async () => {
      const mongo = new mockMongo.Mongo();
      const throttling = {
        throttled: false,
      };

      const url = 'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works';
      const uncommon = { ...FREEFORM_TAG_2, isCommon: false };

      const interaction = new mockDiscordjs.CommandInteraction({ url });
      const handler = new ScrapeHandler(interaction, { mongo, throttling });

      const spies = {
        handleUncommonTag: jest.spyOn(handler, 'handleUncommonTag'),
      };

      mockScrape.scrapeMasterTag.mockReset();
      mockScrape.scrapeMasterTag.mockImplementation(async () => uncommon);

      const tag = await handler.scrapeTag();

      // it should call the uncommon tag handler
      expect(spies.handleUncommonTag).toHaveBeenCalled();

      // it should return null
      expect(tag).toBeNull();
    });
  });
});
