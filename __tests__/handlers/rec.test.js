const EventEmitter = require('events');
const { setTimeout } = require('timers/promises');

const mockDiscordjs = jest.requireActual('../../__mocks__/discord.js');
const mockCollection = jest.requireActual('../../__mocks__/mongodb/collection');
const mockScrape = require('../../__mocks__/src/scrape');

const {
  FANDOM_TAG,
  RELATIONSHIP_TAG,
  SESSION,
  USER,
  WORK,
} = require('../../__mocks__/__data__');

jest.mock('discord.js');
jest.mock('mongodb');
jest.mock('../../src/scrape', () => mockScrape);

const { RATINGS, WARNING_SHORTHANDS } = require('../../src/constants');
const { getRatingsRange, RecHandler } = require('../../src/handlers/rec');
const { Queue } = require('../../src/helpers/queue');
const { Mongo } = require('../../src/mongo');

const { HandlerWithPauseButton } = require('../helpers');

describe('getRatingsRange()', () => {
  it('should return the full RATINGS array if given no arguments', () => {
    const result = getRatingsRange();
    expect(result).toStrictEqual(RATINGS);
  });

  it('should return the full RATINGS array if given undefined arguments', () => {
    const result = getRatingsRange(undefined, undefined, undefined);
    expect(result).toStrictEqual(RATINGS);
  });

  it('should ignore min/max if given a rating value', () => {
    const result = getRatingsRange(RATINGS[1], RATINGS[0], RATINGS[4]);
    expect(result).toStrictEqual([RATINGS[1]]);
  });

  it('should not return values below the min', () => {
    const result = getRatingsRange(undefined, RATINGS[1], undefined);
    expect(result).toStrictEqual(RATINGS.slice(1));
  });

  it('should not return values above the max', () => {
    const result = getRatingsRange(undefined, undefined, RATINGS[3]);
    expect(result).toStrictEqual(RATINGS.slice(0, 4));
  });

  it('should return the correct cross-section when min and max are provided', () => {
    const result = getRatingsRange(undefined, RATINGS[2], RATINGS[4]);
    expect(result).toStrictEqual(RATINGS.slice(2, 5));
  });
});

describe('RecHandler', () => {
  let mongo;
  beforeAll(async () => {
    mongo = new Mongo();
    mongo.init();
    RecHandler.prototype.timeoutAfter = 1000;
  });

  it('should construct a handler', () => {
    const options = {};
    const queues = {};

    const interaction = new mockDiscordjs.CommandInteraction(options);

    const handler = new RecHandler(interaction, { mongo, queues });

    expect(handler.interaction).toBe(interaction);
    expect(handler.mongo).toBe(mongo);
    expect(handler.queues).toBe(queues);
    expect(handler.ratingsRange).toStrictEqual(RATINGS);
  });

  describe('filterWarnings()', () => {
    it('should get warnings by expected value', () => {
      const options = {
        warningoptout: 'show',
        warningviolence: 'hide',
        warningdeath: 'hide',
        warningnoncon: 'require',
      };

      const queues = {};

      const interaction = new mockDiscordjs.CommandInteraction(options);

      const handler = new RecHandler(interaction, { mongo, queues });

      expect(handler.filterWarnings('show')).toStrictEqual([WARNING_SHORTHANDS.warningoptout]);
      expect(handler.filterWarnings('hide')).toStrictEqual([WARNING_SHORTHANDS.warningviolence, WARNING_SHORTHANDS.warningdeath]);
      expect(handler.filterWarnings('require')).toStrictEqual([WARNING_SHORTHANDS.warningnoncon]);
    });
  });

  describe('scrapeRandomFromTag()', () => {
    it('should call scrape and ingest for each found tag', async () => {
      const options = {};
      const queues = {};

      const interaction = new mockDiscordjs.CommandInteraction(options);

      // reset relevant mocks
      mockScrape.scrapeRandomWorks.mockReset();
      mockCollection.find.mockReset();
      mockCollection.updateOne.mockReset();
      mockCollection.bulkWrite.mockReset();

      // mock scrapeRandomWorks scrape
      mockScrape.scrapeRandomWorks.mockImplementationOnce(async () => ({
        works: [WORK],
        pageCount: 5,
      }));
      // mock mongo tag's findTagsByTags find (direct)
      mockCollection.find.mockImplementationOnce(() => ([RELATIONSHIP_TAG]));
      // mock mongo tag's updatePageCount
      mockCollection.updateOne.mockImplementationOnce(async () => {});
      // mock mongo tag's findTagsByTags find (ingestWorks)
      mockCollection.find.mockImplementationOnce(() => ([FANDOM_TAG, RELATIONSHIP_TAG]));
      // mock work's getTopWorksByField (x2)
      mockCollection.find
        .mockImplementationOnce(() => ([WORK]))
        .mockImplementationOnce(() => ([WORK]));
      // mock work's upsertWork
      mockCollection.bulkWrite.mockImplementationOnce(async () => {});

      const handler = new RecHandler(interaction, { mongo, queues });

      const spies = {
        findTagsByTags: jest.spyOn(mongo.tag, 'findTagsByTags'),
      };

      await handler.scrapeRandomFromTag('some tag');

      // it should call "findTagsByTags" with given tag
      const lastCall = spies.findTagsByTags.mock.lastCall[0];
      expect(mockCollection.find).toHaveBeenCalled();
      expect(lastCall).toStrictEqual(['some tag']);
    });
  });

  describe('handle()', () => {
    it('should handle a rec with no option arguments', async () => {
      const options = {};
      const user = { id: USER._id };
      const event = new EventEmitter();
      const queues = {
        randomScrape: new Queue(event),
      };

      const interaction = new mockDiscordjs.CommandInteraction(options, user);

      // reset relevant mocks
      mockCollection.findOne.mockReset();
      mockCollection.aggregate.mockReset();

      // mock mongo user's getUserById findOne
      mockCollection.findOne.mockImplementationOnce(async () => USER);
      // mock mongo session's getSessionById findOne
      mockCollection.findOne.mockImplementationOnce(async () => SESSION);
      // mock Mongo Work's getRandomWorkByScore aggregate
      mockCollection.aggregate.mockImplementationOnce(() => ([WORK]));

      const handler = new RecHandler(interaction, { mongo, queues });
      const spies = {
        validate: jest.spyOn(handler, 'validate'),
        filterWarnings: jest.spyOn(handler, 'filterWarnings'),
        enqueue: jest.spyOn(queues.randomScrape, 'enqueue'),
        getRandomWorkByScore: jest.spyOn(mongo.work, 'getRandomWorkByScore'),
      };

      await handler.handle();

      const lastCallOptions = spies.getRandomWorkByScore.mock.lastCall[0];

      const expectedOptions = {
        hideWarnings: [],
        requireWarnings: [],
        ratingsRange: RATINGS,
        tag: undefined,
      };

      // it should have tried to validate the payload
      expect(spies.validate).toHaveBeenCalled();

      // it should generate a basic options payload for getRandomWorkByScore
      expect(lastCallOptions).toStrictEqual(expectedOptions);

      // it should have tried to filter required and hidden warnings
      expect(spies.filterWarnings).toHaveBeenCalledTimes(2);

      // it should not populate extratags when no extratags are provided
      expect(lastCallOptions).not.toHaveProperty('extratags');

      // it should not try to queue a random scrape without a tag option
      expect(spies.enqueue).not.toHaveBeenCalled();
    });

    it('should handle extratags', async () => {
      const options = { extratags: 'one, two,three' };
      const user = { id: USER._id };

      const queues = {};

      const interaction = new mockDiscordjs.CommandInteraction(options, user);

      // reset relevant mocks
      mockCollection.find.mockReset();
      mockCollection.findOne.mockReset();
      mockCollection.aggregate.mockReset();

      // mock mongo user's getUserById findOne
      mockCollection.findOne.mockImplementationOnce(async () => USER);
      // mock mongo session's getSessionById findOne
      mockCollection.findOne.mockImplementationOnce(async () => SESSION);
      // mock mongo tag's findTagsByTag find
      mockCollection.find.mockImplementationOnce(() => ([FANDOM_TAG]));
      // mock Mongo Work's getRandomWorkByScore aggregate
      mockCollection.aggregate.mockImplementationOnce(() => ([WORK]));

      const handler = new RecHandler(interaction, { mongo, queues });

      const spies = {
        getRandomWorkByScore: jest.spyOn(mongo.work, 'getRandomWorkByScore'),
      };

      await handler.handle();

      const lastCallOptions = spies.getRandomWorkByScore.mock.lastCall[0];

      // it should split extra tags regardless of spacing
      expect(lastCallOptions.additionalTags).toStrictEqual(['one', 'two', 'three']);
    });

    it('should queue a random scrape when tag option is provided', async () => {
      const options = { tag: 'some tag' };
      const user = { id: USER._id };

      const event = new EventEmitter();
      const queues = {
        randomScrape: new Queue(event),
      };

      const interaction = new mockDiscordjs.CommandInteraction(options, user);

      // reset relevant mocks
      mockCollection.find.mockReset();
      mockCollection.findOne.mockReset();
      mockCollection.aggregate.mockReset();

      // mock mongo user's getUserById findOne
      mockCollection.findOne.mockImplementationOnce(async () => USER);
      // mock mongo session's getSessionById findOne
      mockCollection.findOne.mockImplementationOnce(async () => SESSION);
      // mock mongo tag's findTagsByTag find
      mockCollection.find.mockImplementationOnce(() => ([FANDOM_TAG]));
      // mock mongo work's getRandomWorkByScore aggregate
      mockCollection.aggregate.mockImplementationOnce(() => ([WORK]));

      // let's inject a paused handler in the queue first
      const pauseHandler = new HandlerWithPauseButton();
      queues.randomScrape.enqueue('pause baby pause', pauseHandler.handle);

      // create the handler
      const handler = new RecHandler(interaction, { mongo, queues });

      // queue should still be running on the paused job
      expect(queues.randomScrape.running).toBe(true);

      // queue should not have anything else waiting to run yet
      expect(queues.randomScrape.length).toBe(0);

      await handler.handle();

      // it should queue a random scrape for the given tag
      expect(queues.randomScrape.length).toBe(1);

      // prepare to drain queues, which will call scrapeRandomFromTag
      mockScrape.scrapeRandomWorks.mockImplementationOnce(async () => ({
        works: [WORK],
        pageCount: 5,
      }));
      mockCollection.find.mockReset();
      mockCollection.find
        .mockImplementationOnce(() => ([RELATIONSHIP_TAG]))
        .mockImplementationOnce(() => ([FANDOM_TAG, RELATIONSHIP_TAG]))
        .mockImplementationOnce(() => ([WORK]))
        .mockImplementationOnce(() => ([WORK]));

      // drain queues
      pauseHandler.unpause();
      await setTimeout(100);
    });
  });
});
