jest.mock('discord.js');
jest.mock('mongodb');

const mockDiscordjs = jest.requireActual('../../__mocks__/discord.js');
const mockChannel = jest.requireActual('../../__mocks__/discord.js/Channel');
const mockCollection = jest.requireActual('../../__mocks__/mongodb/collection');

const { UserHandler } = require('../../src/handlers/user');
const { Mongo } = require('../../src/mongo');

const { SESSION, USER } = require('../../__mocks__/__data__/index');

describe('UserHandler', () => {
  let mongo;
  beforeAll(async () => {
    mongo = new Mongo();
    mongo.init();
  });

  describe('validateUser()', () => {
    it('should check for a user', async () => {
      const interaction = new mockDiscordjs.CommandInteraction({}, { id: USER._id });
      const app = { mongo };

      const spies = {
        getUserById: jest.spyOn(mongo.user, 'getUserById').mockClear(),
        upsertUser: jest.spyOn(mongo.user, 'upsertUser').mockClear(),
      };

      mockCollection.find.mockImplementationOnce(() => ([{}]));
      mockCollection.findOne.mockImplementationOnce(async () => ({}));

      const handler = new UserHandler(interaction, app);
      await handler.validateUser();

      expect(spies.getUserById).toHaveBeenCalled();
      expect(spies.upsertUser).not.toHaveBeenCalled();
    });

    it('should create a user entry if none exists', async () => {
      const interaction = new mockDiscordjs.CommandInteraction({}, { id: USER._id });
      const app = { mongo };

      const spies = {
        getUserById: jest.spyOn(mongo.user, 'getUserById').mockClear(),
        upsertUser: jest.spyOn(mongo.user, 'upsertUser').mockClear(),
        createSession: jest.spyOn(mongo.session, 'createSession').mockClear(),
      };

      mockCollection.find.mockImplementationOnce(() => ([{}]));
      mockCollection.insertOne.mockImplementationOnce(async () => ({ insertedId: SESSION._id }));
      mockCollection.updateOne.mockImplementationOnce(async () => ({ upsertedId: USER._id }));
      mockCollection.findOne.mockImplementationOnce(async () => null);

      const handler = new UserHandler(interaction, app);
      await handler.validateUser();

      expect(spies.createSession).toHaveBeenCalled();
      expect(spies.getUserById).toHaveBeenCalledTimes(2);
      expect(spies.upsertUser).toHaveBeenCalled();
    });

    it('should create a user session if none exists', async () => {
      const interaction = new mockDiscordjs.CommandInteraction({}, { id: USER._id });
      const app = { mongo };

      const spies = {
        getSessionsByUser: jest.spyOn(mongo.session, 'getSessionsByUser').mockClear(),
        createSession: jest.spyOn(mongo.session, 'createSession').mockClear(),
      };

      mockCollection.findOne.mockImplementationOnce(async () => ({}));
      mockCollection.find.mockImplementationOnce(() => ([]));
      mockCollection.insertOne.mockImplementationOnce(async () => ({ insertedId: SESSION._id }));

      const handler = new UserHandler(interaction, app);
      await handler.validateUser();

      expect(spies.getSessionsByUser).toHaveBeenCalled();
      expect(spies.createSession).toHaveBeenCalled();
    });
  });

  describe('inviteNewUser()', () => {
    it('should send the user a welcome message', async () => {
      const interaction = new mockDiscordjs.CommandInteraction({}, { id: USER._id });
      const app = { mongo };

      mockChannel.makeCode.mockClear();
      mockChannel.send.mockClear();

      mockChannel.makeCode.mockImplementationOnce(() => ({ code: 'test123' }));

      const handler = new UserHandler(interaction, app);
      await handler.inviteNewUser();

      expect(mockChannel.send).toHaveBeenLastCalledWith({ content: 'https://discord.gg/test123' });
    });
  });
});
