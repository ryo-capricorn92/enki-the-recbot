const EventEmitter = require('events');
const { setTimeout } = require('timers/promises');
const { EnkiError, TYPES } = require('../../src/helpers/error');

const { Queue } = require('../../src/helpers/queue');
const { HandlerWithPauseButton } = require('../helpers');

describe('Queue', () => {
  it('should throw an error if an improper event is provided', () => {
    expect(() => new Queue()).toThrow();

    expect(() => new Queue({})).toThrow();
  });

  it('should enqueue provided payloads', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    queue.run = jest.fn();

    expect(queue.length).toBe(0);
    expect(queue.head).toBe(0);
    expect(queue.tail).toBe(0);

    queue.enqueue({}, jest.fn());

    expect(queue.length).toBe(1);
    expect(queue.head).toBe(0);
    expect(queue.tail).toBe(1);
    expect(queue.queue[queue.head]).toBeTruthy();
  });

  it('should trigger a run on enqueue when none is running and the queue is not paused', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    queue.run = jest.fn();
    queue.enqueue({}, jest.fn());

    expect(queue.run).toBeCalled();
  });

  it('should not trigger a run on enqueue if a run is in progress', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    queue.run = jest.fn();
    queue.running = true;
    queue.enqueue({}, jest.fn());

    expect(queue.run).not.toBeCalled();
  });

  it('should not trigger a run on enqueue if queue is paused', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    queue.run = jest.fn();
    queue.paused = true;
    queue.enqueue({}, jest.fn());

    expect(queue.run).not.toBeCalled();
  });

  it('should handle dequeues', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    queue.run = jest.fn();

    const entry = { payload: {}, handler: jest.fn() };
    queue.enqueue(entry.payload, entry.handler);
    queue.enqueue({ test: true }, jest.fn());
    queue.enqueue(entry.payload, entry.handler);

    expect(queue.length).toBe(3);
    expect(queue.head).toBe(0);
    expect(queue.tail).toBe(3);

    const res1 = queue.dequeue();
    expect(res1.payload).toBe(entry.payload);
    expect(res1.handler).toBe(entry.handler);
    expect(queue.length).toBe(2);
    expect(queue.head).toBe(1);
    expect(queue.tail).toBe(3);

    const res2 = queue.dequeue();
    expect(res2.payload).not.toBe(entry.payload);
    expect(res2.handler).not.toBe(entry.handler);
    expect(queue.length).toBe(1);
    expect(queue.head).toBe(2);
    expect(queue.tail).toBe(3);
  });

  it('should handle entries on enqueues', async () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    const runSpy = jest.spyOn(queue, 'run');
    const handler = jest.fn(async () => {
      await setTimeout(10);
    });

    queue.enqueue('one', handler);
    queue.enqueue('two', handler);

    expect(queue.running).toBe(true);

    // wait for the run to finish
    await setTimeout(100);

    expect(queue.running).toBe(false);
    expect(runSpy).toHaveBeenCalled();
    expect(handler).toHaveBeenCalledTimes(2);
    expect(handler).toHaveBeenLastCalledWith('two');
  });

  it('should not allow multiple runners', async () => {
    const HANDLERS = [
      new HandlerWithPauseButton('one'),
      new HandlerWithPauseButton('two'),
      new HandlerWithPauseButton('three'),
      new HandlerWithPauseButton('four'),
      new HandlerWithPauseButton('five'),
    ];

    const event = new EventEmitter();
    const queue = new Queue(event);

    HANDLERS.forEach((handler) => {
      queue.enqueue(handler.name, handler.handle);
    });

    expect(queue.running).toBe(true);
    expect(queue.length).toBe(4);

    const firstRunner = queue.runnerId;
    expect(queue.runnerId).toBe(firstRunner);

    HANDLERS[0].unpause();
    expect(queue.runnerId).toBe(firstRunner);

    // triggering a new runner should replace the old runner
    queue.run();
    expect(queue.runnerId).not.toBe(firstRunner);

    // run order will now be potentially inconsistent
    // let's unpause all the payloads and then assess the runs
    HANDLERS[1].unpause();
    HANDLERS[2].unpause();
    HANDLERS[3].unpause();
    HANDLERS[4].unpause();
    await setTimeout(100);

    // handler should only have been called once with each payload
    expect(HANDLERS[0].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[0].handle).toHaveBeenCalledWith('one');
    expect(HANDLERS[1].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[1].handle).toHaveBeenCalledWith('two');
    expect(HANDLERS[2].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[2].handle).toHaveBeenCalledWith('three');
    expect(HANDLERS[3].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[3].handle).toHaveBeenCalledWith('four');
    expect(HANDLERS[4].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[4].handle).toHaveBeenCalledWith('five');

    // no other runs should be waiting
    expect(queue.length).toBe(0);
  });

  it('should create listeners on "throttle" and "unthrottle" events', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    expect(queue.event).toBe(event);
    expect(event.listenerCount('throttle')).toBe(1);
    expect(event.listenerCount('unthrottle')).toBe(1);
  });

  it('should pause the queue on throttle', async () => {
    const HANDLERS = [
      new HandlerWithPauseButton('one'),
      new HandlerWithPauseButton('two'),
      new HandlerWithPauseButton('three'),
      new HandlerWithPauseButton('four'),
    ];

    const event = new EventEmitter();
    const queue = new Queue(event);

    HANDLERS.forEach((handler) => {
      queue.enqueue(handler.name, handler.handle);
    });

    // queue should be working on the first job
    expect(queue.running).toBe(true);
    expect(queue.length).toBe(3);
    expect(HANDLERS[0].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[1].handle).not.toHaveBeenCalled();

    // if we allow the first job to finish, the next job should be picked up
    HANDLERS[0].unpause();
    await setTimeout(100);
    expect(queue.length).toBe(2);
    expect(HANDLERS[0].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[1].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[2].handle).not.toHaveBeenCalled();

    // if we trigger a throttle, the queue should be paused
    event.emit('throttle');
    expect(queue.paused).toBe(true);

    // if we let the second job finish, the runner should stop and no new job should be picked up
    HANDLERS[1].unpause();
    await setTimeout(100);
    expect(queue.running).toBe(false);
    expect(queue.length).toBe(2);
    expect(HANDLERS[0].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[1].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[2].handle).not.toHaveBeenCalled();
  });

  it('should unpause the queue on unthrottle', () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    expect(queue.paused).toBe(false);

    event.emit('throttle');

    expect(queue.paused).toBe(true);

    event.emit('unthrottle');

    expect(queue.paused).toBe(false);
  });

  it('should handle jobs on unpause', async () => {
    const HANDLERS = [
      new HandlerWithPauseButton('one'),
      new HandlerWithPauseButton('two'),
      new HandlerWithPauseButton('three'),
      new HandlerWithPauseButton('four'),
    ];

    const event = new EventEmitter();
    const queue = new Queue(event);

    HANDLERS.forEach((handler) => {
      queue.enqueue(handler.name, handler.handle);
    });

    event.emit('throttle');
    HANDLERS[0].unpause();
    await setTimeout(100);

    // should not have picked up job two after throttle
    expect(queue.length).toBe(3);
    expect(HANDLERS[0].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[1].handle).not.toHaveBeenCalled();

    event.emit('unthrottle');
    await setTimeout(100);

    // next job should have been picked up after unthrottle
    expect(queue.length).toBe(2);
    expect(HANDLERS[0].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[1].handle).toHaveBeenCalledTimes(1);
    expect(HANDLERS[2].handle).not.toHaveBeenCalled();

    // drain remaining jobs
    HANDLERS[1].unpause();
    HANDLERS[2].unpause();
    HANDLERS[3].unpause();

    jest.useRealTimers();
  });

  it('should handle a throttled error by pausing', async () => {
    const event = new EventEmitter();
    const queue = new Queue(event);

    const error = new EnkiError(TYPES.THROTTLED, { timeout: Date.now() });

    const handler = jest.fn(() => {
      throw error;
    });

    queue.enqueue({}, handler);

    await setTimeout(100);
    expect(queue.paused).toBe(true);
  });
});
